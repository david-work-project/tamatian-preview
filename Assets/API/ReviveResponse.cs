[System.Serializable]

public class ReviveResponse 
{
    public string refid;
    public string code;
    public string desc;
    public ReviveData data;
}
