[System.Serializable]

public class ResetProgressData 
{
    public float turtle_exp;
    public int[] fed_progress;
    public int health_point;
    public int clean_point;
    public int poo_count;
    public int turtle_type_id;
    public string turtle_name;
    public int evo_stage;
}
