using System;
[Serializable]

public class RankerRequestData 
{
    public string refid;
    public string box_id;
    public RankData[] ranker;
}
