[System.Serializable]

public class ResetProgressResponse
{
    public string refid;
    public string code;
    public string desc;
    public ResetProgressData data;
}
