using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using Defective.JSON;


public class API : MonoBehaviour
{

    public bool logText;
    public int index;
    public List<EndPoint> endpoints;

    public string endpoint { get { return endpoints[index].url; } }

    protected UnityWebRequest GetRequest(string url)
    {
        if (logText)
        {
            //Debug.Log("get: " + url);
        }

        var request = UnityWebRequest.Get(url);
        request.SetRequestHeader("content-type", "application/json");

        return request;
    }

    protected UnityWebRequest PostRequest(string url, string json)
    {
        if (logText)
        {
            //Debug.Log("post: " + url);
            //Debug.Log("body: " + json);
        }

        var request = new UnityWebRequest(url, "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(json);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("content-type", "application/json");
        request.timeout = MainManager.singleton.RequestTimeOut;

        return request;
    }
    protected UnityWebRequest PutRequest(string url, string json)
    {
        if (logText)
        {
            //Debug.Log("put: " + url);
            //Debug.Log("body: " + json);
        }

        var request = new UnityWebRequest(url, "PUT");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(json);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("content-type", "application/json");

        return request;
    }

    protected UnityWebRequest PostRequest(string url, JSONObject json)
    {
        return PostRequest(url, json.ToString());
    }

    protected UnityWebRequest PostRequest(string url, WWWForm form)
    {
        if (logText)
        {
            Debug.Log("post: " + url);
        }

        var request = UnityWebRequest.Post(url, form);
        return request;
    }

    protected virtual bool IsError(UnityWebRequest request)
    {
        if (request.responseCode > 200)
            return true;

        if (request.result != UnityWebRequest.Result.Success)
            return true;

        return false;
    }


    [System.Serializable]
    public class EndPoint
    {
        public string label;
        public string url;
    }
}
