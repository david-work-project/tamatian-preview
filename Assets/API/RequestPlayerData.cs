using System;
[Serializable]
public class RequestPlayerData 
{
    public string refid;
    public string language;
    public string box_id;
    public int version;
}
