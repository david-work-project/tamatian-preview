using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class GameAPI : API
{
    [Header("Test")]
    public GetAccessTokenRequest info;
    public RequestPlayerData RequestPlayerDataInfo;
    public string infojson;
    public string playerDataRequestJson;
    public string playerDataJson;
    public string Olddate;
    public GetAccessTokenResponse responsedAccessTokenData; 
    public GetPlayerInfoResponse responsedplayerInfo;
    public SavePlayerProgressData savingPlayerProgressData;
    public RankerRequestData rankerRequestData;
    public SaveResponse savedResponse;
    public ResetProgressRequestData resetProgressRequestData;
    public ResetProgressResponse resetProgressResponse;
    public string apiurl;
    private const string characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    public static GameAPI singleton;
    public int ErrorCount = 0;

    private void Awake()
    {
        singleton = this;
        DontDestroyOnLoad(gameObject);
    }

    public string GenerateRandomString(int length)
    {
        string randomString = "";
        for (int i = 0; i < length; i++)
        {
            int randomIndex = Random.Range(0, characters.Length);
            randomString += characters[randomIndex];
        }
        return randomString;
    }

    public void RequestPlayerData()
    {
        info.mobile_number = MainManager.singleton.UserPhoneNumber;
        string randomizedrefid = GenerateRandomString(20);
        info.refid = randomizedrefid;
        infojson = JsonUtility.ToJson(info);
        Debug.Log(infojson);
        StartCoroutine(GetAccessTokenRequest(infojson));
    }

    IEnumerator GetAccessTokenRequest(string value)
    {
        yield return null;
        var url = apiurl + "/get_access_token";
        var request = PostRequest(url, value);
        yield return request.SendWebRequest();
        responsedAccessTokenData = JsonUtility.FromJson<GetAccessTokenResponse>(request.downloadHandler.text);
        if (request.result != UnityWebRequest.Result.Success)
        {
            ErrorCount += 1;
            if (ErrorCount < 2)
            {
                MainManager.singleton.ShowErrorMessage("Can't Get Access token");
                MainManager.singleton.EnableTap = true;
                RequestPlayerData();
            }
            else
            {
                MainManager.singleton.ErrorShutdown();
                LogManager.singleton.GenerateAPIErrorLog("Can't Get Access token", "GetAccessTokenRequest", "Refid:" + info.refid + "/" + "Code:NULL" + "/" + "Desc:NULL", MainManager.singleton.UserPhoneNumber);
            }
        }
        else
        {
            if (responsedAccessTokenData.data != null)
            {
                Debug.Log(responsedAccessTokenData.data.access_token);
                ErrorCount = 0;
                RequestPlayerDataInfo.refid = info.refid;
                RequestPlayerDataInfo.language = MainManager.singleton._language;
                RequestPlayerDataInfo.box_id = MainManager.singleton._box_id;
                RequestPlayerDataInfo.version = MainManager.singleton.APIVersion;
                playerDataRequestJson = JsonUtility.ToJson(RequestPlayerDataInfo);
                StartCoroutine(GetPlayerDataRequest(playerDataRequestJson));
            }
            else
            {
                if (ErrorCount < 1)
                {
                    ErrorCount += 1;
                    MainManager.singleton.ShowErrorMessage("Wrong Access token Data type recieved");
                    MainManager.singleton.EnableTap = true;
                }
                else
                {
                    MainManager.singleton.ErrorShutdown();
                    LogManager.singleton.GenerateAPIErrorLog("Wrong Access token Data type recieved", "GetAccessTokenRequest", "Refid:" + responsedAccessTokenData.refid + "/" + "Code:" + responsedAccessTokenData.code + "/" + "Desc:" + responsedAccessTokenData.desc, MainManager.singleton.UserPhoneNumber);
                }
            }
        }
    }

    IEnumerator GetPlayerDataRequest(string value)
    {
        yield return null;
        var url = apiurl + "/get_account_info";
        var request = PostRequest(url, value);
        request.SetRequestHeader("authorization", "Bearer " + responsedAccessTokenData.data.access_token);
        yield return request.SendWebRequest();
        responsedplayerInfo = JsonUtility.FromJson<GetPlayerInfoResponse>(request.downloadHandler.text);

        if (request.result != UnityWebRequest.Result.Success)
        {
            ErrorCount += 1;
            if (ErrorCount < 2)
            {
                MainManager.singleton.ShowErrorMessage("Can't Get Player Data");
                MainManager.singleton.EnableTap = true;
            }
            else
            {
            MainManager.singleton.ErrorShutdown();
            LogManager.singleton.GenerateAPIErrorLog("Can't Get Player Data", "GetPlayerDataRequest", "Refid:" + info.refid + "/" + "Code:NULL" + "/" + "Desc:NULL", MainManager.singleton.UserPhoneNumber);
            }
        }
        else {
            if (responsedplayerInfo.data != null)
            {
                MainManager.singleton.ReceivedDataFromAPI();
            }
            else
            {
                MainManager.singleton.ShowErrorMessage("Wrong Player Data type recieved");
                LogManager.singleton.GenerateAPIErrorLog("Wrong Player Data type recieved", "GetPlayerDataRequest", "Refid:" + responsedplayerInfo.refid + "/" + "Code:" + responsedplayerInfo.code + "/" + "Desc:" + responsedplayerInfo.desc, MainManager.singleton.UserPhoneNumber);
            }
        }
    }

    public void SavePlayerData()
    {
        var mainM = MainManager.singleton;

        savingPlayerProgressData.refid = info.refid;
        savingPlayerProgressData.account_level = mainM.AccountLevel;
        savingPlayerProgressData.account_exp = mainM.AccountExp;
        savingPlayerProgressData.turtle_exp = mainM.TurtleExp; 
        savingPlayerProgressData.turtorial_stage = mainM.OnTutorialStage;
        savingPlayerProgressData.fed_progress = mainM.FedProgress; 
        savingPlayerProgressData.health_point = mainM.Fullness; 
        savingPlayerProgressData.clean_point = mainM.Clean; 
        savingPlayerProgressData.poo_count = mainM.PooCount; 
        savingPlayerProgressData.played_bonus = mainM.PlayedToday;
        savingPlayerProgressData.played_bonus = mainM._played_bonus;
        savingPlayerProgressData.language = mainM._language;
        savingPlayerProgressData.current_background = mainM.CurrentBackground;
        savingPlayerProgressData.background_collection = mainM.BackgroundCollection;
        savingPlayerProgressData.turtle_type_id = mainM.TurtleType_ID;
        savingPlayerProgressData.turtle_collection = mainM.TurtleCollection;
        savingPlayerProgressData.tao_currency = mainM.TaoCurrency;
        savingPlayerProgressData.version = mainM.APIVersion;

        string SaveJson = JsonUtility.ToJson(savingPlayerProgressData);
        StartCoroutine(SavePlayerDataRequest(SaveJson));

    }

    public void SaveRankData()
    {
        rankerRequestData.refid = info.refid;
        rankerRequestData.box_id = MainManager.singleton._box_id;
        rankerRequestData.ranker = MainManager.singleton.NewRankData.ToArray();
        string rankerJson = JsonUtility.ToJson(rankerRequestData);
        StartCoroutine(SaveRankDataRequest(rankerJson));
    }

    IEnumerator SavePlayerDataRequest(string value)
    {
       
        yield return null;
        var url = apiurl + "/save_game_data";
        var request = PostRequest(url, value);
        request.SetRequestHeader("authorization", "Bearer " + responsedAccessTokenData.data.access_token);
        yield return request.SendWebRequest();
        savedResponse = JsonUtility.FromJson<SaveResponse>(request.downloadHandler.text);

        if (request.result != UnityWebRequest.Result.Success)
        {
            ErrorCount += 1;
            if (ErrorCount < 2)
            {
                SavePlayerData();
            }
            else
            {
            MainManager.singleton.ErrorShutdown();
            LogManager.singleton.GenerateAPIErrorLog("Can't Save Player Data", "SavePlayerDataRequest", "Refid:" + info.refid + "/" + "Code:NULL" + "/" + "Desc:NULL", MainManager.singleton.UserPhoneNumber);
             }
        }
        else if (savedResponse != null) 
        {
            EndingSceneManager.singleton.ShowEndPanel();
        }
    }

    IEnumerator SaveRankDataRequest(string value)
    {
        yield return null;
        var url = apiurl + "/save_rank_data";
        var request = PostRequest(url, value);
        request.SetRequestHeader("authorization", "Bearer " + responsedAccessTokenData.data.access_token);

        yield return request.SendWebRequest();
        savedResponse = JsonUtility.FromJson<SaveResponse>(request.downloadHandler.text);

        if (request.result != UnityWebRequest.Result.Success)
        {
            ErrorCount += 1;
            if (ErrorCount < 2)
            {
                SaveRankData();
            }
            else
            {

                MainManager.singleton.ErrorShutdown();
                LogManager.singleton.GenerateAPIErrorLog("Can't Save Rank Data", "SaveRankDataRequest", "Refid:" + info.refid + "/" + "Code:NULL" + "/" + "Desc:NULL", MainManager.singleton.UserPhoneNumber);
            }
        }
    }
    public void ResetGame()
    {
       resetProgressRequestData.refid = info.refid;
        string ResetJson = JsonUtility.ToJson(resetProgressRequestData);
        StartCoroutine(ResetPlayerDataRequest(ResetJson));
    }

    IEnumerator ResetPlayerDataRequest(string value)
    {
        yield return null;
        var url = apiurl + "/reset_progress";
        var request = PostRequest(url, value);
        request.SetRequestHeader("authorization", "Bearer " + responsedAccessTokenData.data.access_token);
        yield return request.SendWebRequest();
       
        if (request.result == UnityWebRequest.Result.Success)
        {
            resetProgressResponse = JsonUtility.FromJson<ResetProgressResponse>(request.downloadHandler.text);
        }
        else 
        {
            ErrorCount += 1;
            if (ErrorCount < 2)
            {
                ResetGame();
            }
            else
            {
            MainManager.singleton.ErrorShutdown();
            LogManager.singleton.GenerateAPIErrorLog("Can't Reset Game", "ResetGameRequest", "Refid:" + info.refid + "/" + "Code:NULL" + "/" + "Desc:NULL", MainManager.singleton.UserPhoneNumber);
            }
        }
    }
}
