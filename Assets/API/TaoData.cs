
using System;

[Serializable]
public class TaoData 
{
    public string refid;
    public string mobile_number;
    public int account_level;
    public float account_exp;
    public int[] account_exp_table;
    public float turtle_exp;
    public int[] turtle_exp_table;
    public int tutorial_stage;
    public int[] fed_progress;
    public int health_point;
    public int clean_point;
    public int poo_count;
    public bool played_bonus;
    public bool played_today;
    public string language;
    public int current_background;
    public int[] background_collection;
    public int turtle_type_id;
    public string turtle_name;
    public int evo_stage;
    public int[] turtle_collection;
    public int tao_currency;
}
