[System.Serializable]

public class GetPlayerInfoResponse 
{
    public string refid;
    public string code;
    public string desc;
    public ResponsedPlayerInfoData data;
}
