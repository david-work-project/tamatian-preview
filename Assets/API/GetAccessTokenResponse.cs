[System.Serializable]

public class GetAccessTokenResponse 
{
    public string refid;
    public string code;
    public string desc;
    public AccessTokenResponseData data;

}
