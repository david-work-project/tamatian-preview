using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggHatchingStateEnd : StateMachineBehaviour
{
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
       Egg egg = animator.gameObject.GetComponent<Egg>();
        if (egg.OnHatch)
        {
            GameplayManager.singleton.EggHatching();
        }
    }
}
