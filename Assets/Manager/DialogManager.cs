using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class DialogManager : MonoBehaviour
{
    public static DialogManager singleton;

    [Header("Thai Dialogue")]
    public string TutorialStepOneTHDialog;
    public string TutorialStepTwoTHDialog;
    public string TutorialStepThreeTHDialog;
    public string PlayTHDialog;
    public string SmallDirtyTHDialog;
    public string MediumDirtyTHDialog;
    public string VeryDirtyTHDialog;
    public string DeadTHDialog;
    public string PooFormTHDialog;
    public string MoreDrinkRequiredTHDialog;

    [Header("English Dialogue")]
    public string TutorialStepOneENDialog;
    public string TutorialStepTwoENDialog;
    public string TutorialStepThreeENDialog;
    public string PlayENDialog;
    public string SmallDirtyENDialog;
    public string MediumDirtyENDialog;
    public string VeryDirtyENDialog;
    public string DeadENDialog;
    public string PooFormENDialog;
    public string MoreDrinkRequiredENDialog;

    [Header("Current Dialogue")]
    public string TutorialStepOneDialog;
    public string TutorialStepTwoDialog;
    public string TutorialStepThreeDialog;
    public string PlayDialog;
    public string SmallDirtyDialog;
    public string MediumDirtyDialog;
    public string VeryDirtyDialog;
    public string DeadDialog;
    public string PooFormDialog;
    public string MoreDrinkRequiredDialog;


    private void Awake()
    {
        singleton = this;
        GetLanguageFromSettings();
    }

    void GetLanguageFromSettings()
    {
        switch (MainManager.singleton._language)
        {
            case "TH":
                TutorialStepOneDialog = TutorialStepOneTHDialog;
                TutorialStepTwoDialog = TutorialStepTwoTHDialog;
                TutorialStepThreeDialog = TutorialStepThreeTHDialog;
                PlayDialog = PlayTHDialog;
                SmallDirtyDialog = SmallDirtyTHDialog;
                MediumDirtyDialog = MediumDirtyTHDialog;
                VeryDirtyDialog = VeryDirtyTHDialog;
                DeadDialog = DeadTHDialog;
                PooFormDialog = PooFormTHDialog;
                MoreDrinkRequiredDialog = MoreDrinkRequiredTHDialog;
                break;
            case "EN":
                TutorialStepOneDialog = TutorialStepOneENDialog;
                TutorialStepTwoDialog = TutorialStepTwoENDialog;
                TutorialStepThreeDialog = TutorialStepThreeENDialog;
                PlayDialog = PlayENDialog;
                SmallDirtyDialog = SmallDirtyENDialog;
                MediumDirtyDialog = MediumDirtyENDialog;
                VeryDirtyDialog = VeryDirtyENDialog;
                DeadDialog = DeadENDialog;
                PooFormDialog = PooFormENDialog;
                MoreDrinkRequiredDialog = MoreDrinkRequiredENDialog;
                break;
        }
    }

    public void FeedTutorialDialog()
    {
        
        Pet.instance.DialogBalloon.ChangeDialog(TutorialStepOneDialog);
        Pet.instance.DialogBalloon.FeedButton.gameObject.SetActive(true);
    }
    public void CleanTutorialDialog()
    {
       
        Pet.instance.DialogBalloon.ChangeDialog(TutorialStepTwoDialog);
    }
    public void PlayMiniGameTutorialDialog()
    {
        
        Pet.instance.DialogBalloon.ChangeDialog(TutorialStepThreeDialog);
        Pet.instance.DialogBalloon.PlayButton.gameObject.SetActive(true);

    }

    public void PoolFormDialog()
    {
        
        Pet.instance.DialogBalloon.ChangeDialog(PooFormDialog);
        Pet.instance.DialogBalloon.PlayButton.gameObject.SetActive(true);
    }
    
    public void OnDeadDialog()
    {
        
        Pet.instance.DialogBalloon.ChangeDialog(DeadDialog);
        Pet.instance.DialogBalloon.PlayButton.gameObject.SetActive(true);
    }

    public void SmallAmountOfPooDialog()
    {
        
        Pet.instance.DialogBalloon.ChangeDialog(SmallDirtyDialog);
        Pet.instance.DialogBalloon.SkipButton.gameObject.SetActive(true);
    }

    public void MediumAmountOfPooDialog()
    {
       
        Pet.instance.DialogBalloon.ChangeDialog(MediumDirtyDialog);
        Pet.instance.DialogBalloon.SkipButton.gameObject.SetActive(true);
    }

    public void LargeAmountOfPooDialog()
    {
       
        Pet.instance.DialogBalloon.ChangeDialog(VeryDirtyDialog);
        Pet.instance.DialogBalloon.SkipButton.gameObject.SetActive(true);
    }

    public void NeedMoreDrinkDialog()
    {
       
        Pet.instance.DialogBalloon.ChangeDialog(MoreDrinkRequiredDialog);
        Pet.instance.DialogBalloon.PlayButton.gameObject.SetActive(true);
    }

    public void PlayMiniGameDialog()
    {
        Pet.instance.DialogBalloon.ChangeDialog(PlayDialog);
        Pet.instance.DialogBalloon.PlayButton.gameObject.SetActive(true);
    }

}
