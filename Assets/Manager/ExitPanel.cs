using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitPanel : MonoBehaviour
{
    public void CancelExit()
    {
        GameplayManager.singleton.ExitPanel.SetActive(false);
        GameplayManager.singleton.OnQuitConfirmPanel = false;
    }

    public void ExitButton()
    {
        MainManager.singleton.ExitingGame();
    }
}
