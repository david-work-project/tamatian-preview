using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InputPhoneNumber : MonoBehaviour
{
   
    public TMP_InputField InputField;
    public GameObject Invalid;
    public string phoneNumber;
    public int visibleChar = 4;
    // Start is called before the first frame update
    private void Awake()
    {
        
       
    }
    private void OnEnable()
    {

      //  InputField.onValueChanged.AddListener(FormatPhoneNumber);
    }
    void Start()
    {
       phoneNumber = MainManager.singleton.UserPhoneNumber;
        
        if (phoneNumber.Length > visibleChar)
        {
            int replaceCharacterLength = phoneNumber.Length - visibleChar;
            string replacement = new string('X', replaceCharacterLength);
            string result = replacement + phoneNumber.Substring(replaceCharacterLength);
            result = result.Insert(3, "-");
            result = result.Insert(7, "-");
            InputField.text = result;
        }
        //FormatPhoneNumber(phoneNumber);

        


    }



    void FormatPhoneNumber(string text)
    {
        // Remove all non-digit characters from the input
       string digitsOnly = Regex.Replace(text, @"[^\d]", "");
       

        // Add dashes to the input
        if (digitsOnly.Length >= 4 && digitsOnly.Length <= 7)
        {
            InputField.text = digitsOnly.Substring(0, 3) + "-" + digitsOnly.Substring(3);
        }
        else if (digitsOnly.Length > 7)
        {
            InputField.text = digitsOnly.Substring(0, 3) + "-" + digitsOnly.Substring(3, 3) + "-" + digitsOnly.Substring(6);
        }
        else
        {
            InputField.text = digitsOnly;
        }

        // Move the caret to the end of the input
        InputField.caretPosition = InputField.text.Length;
    }

    public void NumberButton(string Number)
    {
        if (InputField.text.Length < 12)
        {
            if (InputField.text.Length < 7)
            {
                if (InputField.text.Length == 3 )
                {

                    InputField.text += "-X" ;

                }
                else
                {
                    InputField.text += "X";
                }
            }
            else
            {
                if (InputField.text.Length == 7)
                {
                    InputField.text += "-" + Number;
                }
                else
                {
                    InputField.text += Number;
                }
            }
          
            phoneNumber += Number;
        }
        
    } 

    public void DeleteNumber() {
        if (InputField.text.Length <= 0) return;
        if (InputField != null && InputField.text[InputField.text.Length - 1] == '-')
        {
            InputField.text = InputField.text.Substring(0, InputField.text.Length - 2);
        }
        else
        {
            InputField.text = InputField.text.Substring(0, InputField.text.Length - 1);
        }
        phoneNumber = phoneNumber.Substring(0, phoneNumber.Length - 1);
        
    }

    public string RemoveDashes()
    {
        if (InputField != null)
        {
            var RemoveDashesNumber  = InputField.text.Replace("-", "");
            Debug.Log(RemoveDashesNumber);
            return RemoveDashesNumber;
        }
        return "";
    }

    public void EnterNumber()
    {
        if(InputField.text.Length < 12)
        {
           Invalid.SetActive(true);
        }
        else
        {
            MainManager.singleton.UserPhoneNumber = phoneNumber;
            MainManager.singleton.ReceivedNumber();
            gameObject.transform.parent.gameObject.SetActive(false);
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
