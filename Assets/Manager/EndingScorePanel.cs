using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EndingScorePanel : MonoBehaviour
{
    public RankBlog rankBlogPrefabs;
    public Transform container;

    
    void Start()
    {
        foreach (var rank in MainManager.singleton.ShowRankData)
        {
            var rankBlog = Instantiate(rankBlogPrefabs, container);
            rankBlog.Rank.text = rank.rank.ToString();
            rankBlog.Name.text = rank.name;
            if (rank.mobile_number == MainManager.singleton.UserPhoneNumber)
            {
                var CurrentSessionScore = Mathf.FloorToInt(MainManager.singleton.Score);
                rankBlog.Name.text = "You";
                if(rank.score == CurrentSessionScore)
                {
                    rankBlog.CurrentSessionHighlight.SetActive(true);
                }
                else
                {
                    rankBlog.Highlight.SetActive(true);
                }
                
            }
            rankBlog.Score.text = rank.score.ToString();
        }
    }

}
