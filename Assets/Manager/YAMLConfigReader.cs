using UnityEngine;
using System.IO;
using System.Collections.Generic;
using YamlDotNet.Serialization;


public static class YAMLConfigReader 
{
    public static string GetStringValueFromConfig(string filePath, string valueName , string defaultValue)
    {
        if (string.IsNullOrEmpty(filePath))
            return defaultValue;

        if (File.Exists(filePath) == false)
            return defaultValue;

        using (StreamReader reader = new StreamReader(filePath))
        {
            string yamlText = reader.ReadToEnd();

            Deserializer deserializer = new Deserializer();
            var parsedData = deserializer.Deserialize<Dictionary<string, string>>(new StringReader(yamlText));

            if (parsedData.ContainsKey(valueName))
            {
                string StringValue = parsedData[valueName];

                return StringValue;
            }
        }
        return defaultValue;
    }

   public static int GetIntValueFromConfig(string filePath, string valueName, int defaultValue)
    {
        if (string.IsNullOrEmpty(filePath))
            return defaultValue;

        if (File.Exists(filePath) == false)
            return defaultValue;

        using (StreamReader reader = new StreamReader(filePath))
        {
            string yamlText = reader.ReadToEnd();

            Deserializer deserializer = new Deserializer();
            var parsedData = deserializer.Deserialize<Dictionary<string, string>>(new StringReader(yamlText));

            if (parsedData.ContainsKey(valueName))
            {
                string stringValue = parsedData[valueName];
                int intValue = int.Parse(stringValue);

                return intValue;
            }
        }
        return defaultValue;
    }

    public static float GetFloatValueFromConfig(string filePath, string valueName, float defaultValue)
    {
        if (string.IsNullOrEmpty(filePath))
            return defaultValue;

        if (File.Exists(filePath) == false)
            return defaultValue;

        using (StreamReader reader = new StreamReader(filePath))
        {
            string yamlText = reader.ReadToEnd();

            Deserializer deserializer = new Deserializer();
            var parsedData = deserializer.Deserialize<Dictionary<string, string>>(new StringReader(yamlText));

            if (parsedData.ContainsKey(valueName))
            {
                string stringValue = parsedData[valueName];
                float floatValue = float.Parse(stringValue);

                return floatValue;
            }
        }
        return defaultValue;
    }

}
