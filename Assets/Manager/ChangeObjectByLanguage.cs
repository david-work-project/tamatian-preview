using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class ChangeObjectByLanguage : MonoBehaviour
{
    public GameObject TH;
    public GameObject EN;
    public string Language
    {
        get { return MainManager.singleton._language; }
        set
        {
            if (MainManager.singleton._language != value)
            {
                Debug.Log("Change Language to: " + value);
                MainManager.singleton._language = value;
                Change();
            }
        }
    }

    void Start()
    {
        
        switch (MainManager.singleton._language)
        {
            case "TH":
                TH.gameObject.SetActive(true);
                EN.gameObject.SetActive(false);
                break;
            case "EN":
                TH.gameObject.SetActive(false);
                EN.gameObject.SetActive(true);
                break;
            default:
                TH.gameObject.SetActive(true);
                EN.gameObject.SetActive(false);
                break;

        }
    }
    public void Change()
    {
        switch (MainManager.singleton._language)
        {
            case "TH":
                TH.gameObject.SetActive(true);
                EN.gameObject.SetActive(false);
                break;
            case "EN":
                TH.gameObject.SetActive(false);
                EN.gameObject.SetActive(true);
                break;
            default:
                TH.gameObject.SetActive(true);
                EN.gameObject.SetActive(false);
                break;

        }
    }


}
