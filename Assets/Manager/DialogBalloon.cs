using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DialogBalloon : MonoBehaviour
{
    public TextMeshProUGUI DialogText;
    public string TextToChange;
    public Coroutine AnimateDialogCoroutine;
    public GameObject FeedButton;
    public GameObject SkipButton;
    public GameObject PlayButton;
    public void ChangeDialog(string dialogToChange)
    {
        if (AnimateDialogCoroutine != null)
        {
            StopCoroutine(AnimateDialogCoroutine);
        }
        Pet.instance.DialogBalloon.gameObject.SetActive(true);
        DialogText.text = "";
        TextToChange = dialogToChange;
        AnimateDialogCoroutine = StartCoroutine(AnimateDialog());
    }

    IEnumerator AnimateDialog()
    {
        foreach (char letter in TextToChange.ToCharArray())
        {
            DialogText.text += letter;
            yield return new WaitForSeconds(0.05f);
        }
    }
}
