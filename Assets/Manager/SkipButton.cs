using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkipButton : MonoBehaviour
{
    public void SkipButtonClick()
    {
        GameplayManager.singleton.ShowFeedPanel();
        gameObject.SetActive(false);
        gameObject.transform.parent.gameObject.SetActive(false);

    }
}
