using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Mathematics;
using UnityEngine;

public class ScoreIndicatorControl : MonoBehaviour
{
    public Animator ScoreAnim;
    public TextMeshProUGUI ScoreText;
    public float TurnToDefaultTime;
    public GameObject XPText;
    public GameObject GainText;
    public TextMeshProUGUI MultiplyText;
    public TextMeshProUGUI MultiplyLabel;
    public TextMeshProUGUI ComboText;
    public TextMeshProUGUI ComboLabel;
    public string AnimName;
    public ParticleSystem Goodparticle;
    public ParticleSystem Badparticle;
    
    public void ReceivedScore(float RecievedNumber)
    {
        if (IsInvoking()) CancelInvoke("TurnToDefault");
        if (RecievedNumber > 0)
        {
            ScoreText.color = FoodRainingManager.singleton.CurrentColor;
            Goodparticle.Stop();
            Goodparticle.Play();
        }
        else
        {
            ScoreText.color = FoodRainingManager.singleton.NegativeScoreColor;
            Badparticle.Stop();
            Badparticle.Play();
            Invoke("TurnToDefault", TurnToDefaultTime);
        }
     
        ScoreText.text = math.floor(MainManager.singleton.Score).ToString();
        ComboText.text = FoodRainingManager.singleton.ComboCount.ToString() ;
        ScoreAnim.Play(AnimName);
    }

    public void RecievedFeedXP(float RecievedNumber)
    {
        ScoreText.text = RecievedNumber.ToString("F1");
    }

    public void TurnToDefault()
    {
        if (MainManager.singleton.Score >= 0)
        {
            ScoreText.color = FoodRainingManager.singleton.PositiveScoreColor;
        }
        else
        {
            ScoreText.color = FoodRainingManager.singleton.NegativeScoreColor;
        }
    }

}
