using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class CutsceneManager : MonoBehaviour
{
    public VideoPlayer[] VideoList;
    public VideoPlayer CutscenePlayer;
    public GameObject SkipBtn;
    public Coroutine FadeInCoroutine;
    public Coroutine FadeOutCoroutine;
    public bool Reached90Percent;
    void Start()
    {
        int videoIndex = MainManager.singleton.CutsceneToPlay;
        if(videoIndex == 0) SkipBtn.SetActive(true);
        CutscenePlayer = Instantiate(VideoList[videoIndex],transform);
        CutscenePlayer.targetCamera = Camera.main;
        //CutscenePlayer = VideoList[videoIndex];

        CutscenePlayer.loopPointReached += OnCutsceneEnd;



    }

    public void StartPlayVideo(int VideoToplay)
    {
        if (VideoToplay < 1)
        {
            SkipBtn.SetActive(true);

        }


       
        FadeInCoroutine = StartCoroutine("FadeIn");
      //  CutscenePlayer.gameObject.SetActive(false);
        



    }

    //public IEnumerator FadeIn()
    //{
    //    var t = 1f;
    //    var speed = 1/t;
    //    var a = CutsceneScreen.color;
    //    while(a.a< 1f)
    //    {
    //        a.a += speed * Time.deltaTime;
    //        //CutsceneScreen.color += new Color(0, 0, 0, 0.01f);
    //       CutsceneScreen.color=a;
    //        yield return null;
                
    //    }
       
        
    //}

    //public IEnumerator FadeOut()
    //{
    //    Debug.Log("CallFade");
    //    while (CutsceneScreen.color.a > 0)
    //    {
    //        CutsceneScreen.color -= new Color(0, 0, 0, 0.01f);
    //        yield return null;

    //    }
       
    //    CutscenePlayer.gameObject.SetActive(false);
    //    this.gameObject.SetActive(false);
    //    Debug.Log("StopCall");
    //}

    private void OnCutsceneEnd(VideoPlayer vp)
    {
        if (MainManager.singleton.OnDead != true)
        {
            MainManager.singleton.GoToGameplay();
        }
        else
        {
            MainManager.singleton.ShowDeadPanel();
        }
       // CutscenePlayer.gameObject.SetActive(false);
       
       
       
        
      
        
    }
    public void SkipCutscene()
    {
        MainManager.singleton.GoToGameplay();
    }

    private void Update()
    {
    //    if(!Reached90Percent&&CutscenePlayer.isPlaying && CutscenePlayer.time/ CutscenePlayer.length >= 0.9)
    //    {
    //        Reached90Percent = true;
    //        OnCutscene90Percent();
    //    }
    }

    //public void OnCutscene90Percent()
    //{
    //  //  Debug.Log(CutsceneScreen.color.a);
    // //   StopCoroutine(FadeInCoroutine);
        
    //    MainManager.singleton.GameplayGroup.gameObject.SetActive(true);
    //    if (GameplayManager.singleton)
    //    {
    //        if (GameplayManager.singleton.isDead)
    //        {

    //            MainManager.singleton.ExitingGame();
    //        }
           
    //        FadeOutCoroutine = StartCoroutine("FadeOut");
    //        GameplayManager.singleton.FinishedCutscene();
    //        CutscenePlayer.gameObject.SetActive(false);
    //        this.gameObject.SetActive(false);
    //        Debug.Log("StopCall");

    //    }
    //    else
    //    {
    //        MainManager.singleton.GoToGameplay();
    //          this.gameObject.SetActive(false);
    //    }
    //}










}
