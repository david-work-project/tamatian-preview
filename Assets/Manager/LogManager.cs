using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using static UnityEngine.Application;

public class LogManager : MonoBehaviour
{
    public static LogManager singleton;
    public string FileHeadname;
    public string ErrorHeadname;
    public string APIErrorTypeName;
    private string path;
    public string editorPath;
    public string AndroidPath;
    public string logFileName;
    public string ErrorFileName;
    public string logFilePath;
    public string ErrorFilePath;
    public string cachedNumber;

    private void Awake()
    {
        if (singleton == null)
        {
            singleton = this;
            DontDestroyOnLoad(this.gameObject);

        }
        else
        {
            Destroy(this.gameObject);
        }
        path = Application.dataPath+ "/" +editorPath+"/";
   #if UNITY_ANDROID && !UNITY_EDITOR     

   path = AndroidPath; 

    #endif
    }

    void Start()
    {
        string dateTime = DateTime.Now.ToString("yyyyMMdd");
        logFileName = FileHeadname + dateTime + ".log";
        logFilePath = path + logFileName;
        ErrorFileName = ErrorHeadname + dateTime + ".log";
        ErrorFilePath = path + ErrorFileName;
        if (!Directory.Exists(path))
        {
            try
            {
                Directory.CreateDirectory(path);
            }
            catch (Exception e)
            {
                Debug.LogError("Failed to create directory: " + e.ToString());
                return;
            }
        }
        Application.logMessageReceived += LogCallback;
    }

  

    public void GenerateUserEnterLog( string phone_number )
    {

        string hourdatetime = DateTime.Now.ToString("HH:mm:ss");

        if (!File.Exists(logFilePath))
        {
            try
            {
                if (phone_number != null || phone_number != "")
                {
                    File.WriteAllText(logFilePath, hourdatetime + "/" + phone_number + "/" + Application.version);
                }
                else
                {
                    File.WriteAllText(logFilePath, hourdatetime + "/" + "UNKNOWN" + "/" + Application.version);
                }
            }
            catch (Exception e)
            {
                Debug.LogError("Failed to create directory: " + e.ToString());
                return;
            }
        }
        else
        {
            if (phone_number != null || phone_number != "")
            {
                File.AppendAllText(logFilePath, "\n" + hourdatetime + "/" + phone_number + "/" + Application.version);
            }
            else
            {
                File.AppendAllText(logFilePath, "\n" + hourdatetime + "/" + "UNKNOWN" + "/" + Application.version);
            }
        }
    }

    public void WriteUserLoginLog(string phone_number)
    {

        string hourdatetime = DateTime.Now.ToString("HH:mm:ss");
        
        if (File.Exists(logFilePath))
        {
            File.AppendAllText(logFilePath, "/" + phone_number);
        }
        else
        {
            try
            {
                if (phone_number != null || phone_number != "")
                {
                    File.WriteAllText(logFilePath, hourdatetime + "/" + phone_number + "/" + Application.version + "/" + phone_number);
                }
                else
                {
                    File.WriteAllText(logFilePath, hourdatetime + "/" + "UNKNOWN" + "/" + Application.version + "/" + phone_number);
                }
            }
            catch (Exception e)
            {
                Debug.LogError("Failed to create directory: " + e.ToString());
                return;
            }
            
        }
        
    }

    public void GenerateAPIErrorLog(string error_Name , string request_name , string error_response , string phone_number)
    {
        
        string userNumber = "UNKNOWN";
        if (phone_number != null || phone_number != "")
        {
            userNumber = phone_number;
        }
        
        string hourdatetime = DateTime.Now.ToString("HH:mm:ss");
        if (File.Exists(ErrorFilePath))
        {
            File.AppendAllText(ErrorFilePath, "\n" + hourdatetime + "/" + userNumber +"/" + APIErrorTypeName+ "/" + error_Name + "/" + request_name + "/" + error_response);
        }
        else
        {
            try
            {
                File.WriteAllText(ErrorFilePath, hourdatetime + "/" + userNumber + "/" + APIErrorTypeName + "/" + error_Name + "/" + request_name + "/" + error_response);
            }
            catch (Exception e)
            {
                Debug.LogError("Failed to create directory: " + e.ToString());
                return;
            }
        }
    }
    void LogCallback(string condition, string stackTrace, LogType type)
    {
        string userNumber = "UNKNOWN";
        if (MainManager.singleton.UserPhoneNumber != null || MainManager.singleton.UserPhoneNumber != "")
        {
            userNumber = MainManager.singleton.UserPhoneNumber;
        }
        string hourdatetime = DateTime.Now.ToString("HH:mm:ss");
        if (type == LogType.Error || type == LogType.Exception)
        {
            // Format the error message
            string errorMessage = $"Error: {condition}\nStackTrace: {stackTrace}\n";

            if (File.Exists(ErrorFilePath))
            {
                File.AppendAllText(ErrorFilePath,  hourdatetime + "/" + userNumber + "/" + errorMessage);
            }
            else
            {
                try
                {
                    File.WriteAllText(ErrorFilePath, hourdatetime + "/" + userNumber + "/" + errorMessage);
                }
                catch (Exception e)
                {
                    Debug.LogError("Failed to create directory: " + e.ToString());
                    return;
                }
            }
        }
    }

}
