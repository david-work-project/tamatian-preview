using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnParticleToPool : MonoBehaviour
{
    public float TimeToReturn = 1.5f;
    public string PoolName;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnEnable()
    {
        Invoke("ReturnToPool", TimeToReturn);
    }

    private void ReturnToPool()
    {
        FoodRainingManager.singleton.ReturnParticleToPool(PoolName,gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
