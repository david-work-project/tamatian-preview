using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PooManager : MonoBehaviour
{
    public static PooManager singleton;
    public GameObject Poo;
    public GameObject[] PooSpawnArea;
    public GameObject TutorialPooSpawnLocation;
    public GameObject PointerPoo;

    private void Awake()
    {
        singleton = this;
    }
    public void SpawnPoo(int NumberOfPoo)
    {
        for (int i = 0; i < NumberOfPoo; i++)
        {
            int wheretospawn = Random.Range(0, PooSpawnArea.Length);
            Vector3 randomPosition;
            Vector3 Position = PooSpawnArea[wheretospawn].transform.position;
            float HalfWidthArea = PooSpawnArea[wheretospawn].transform.localScale.x / 2;
            float HalfHeightArea = PooSpawnArea[wheretospawn].transform.localScale.y / 2;

            randomPosition = new Vector3(
          Random.Range(Position.x - HalfWidthArea, Position.x + HalfWidthArea),
          Random.Range(Position.y - HalfHeightArea, Position.y + HalfHeightArea),
          0
          );

            GameObject PooInGame = Instantiate(Poo, randomPosition, Quaternion.identity, transform);
        }
    }
    public void SpawnPooForTutorial()
    {
        Vector3 TutorialPooSpawnArea = TutorialPooSpawnLocation.transform.position;
        MainManager.singleton.PooCount = 1;
        GameObject PooInGame = Instantiate(Poo, TutorialPooSpawnLocation.transform.position, Quaternion.identity, transform);
        PointerPoo.gameObject.SetActive(true);
    }

    public void PickUpPoo()
    {
        MainManager.singleton.PooCount -= 1;
        if (MainManager.singleton.PooCount <= 0) MainManager.singleton.PooCount = 0;
        if (MainManager.singleton.OnTutorialStage > 3)
        {
            if (MainManager.singleton.PooCount <= 0)
            {
               
                    MainManager.singleton.Clean += 30;
                    if (MainManager.singleton.Clean > 100) MainManager.singleton.Clean = 100;
                    GameplayManager.singleton.AllMeterGroup.OnCleanMeterChange();
                    if (MainManager.singleton.FedThisLaunch != true) GameplayManager.singleton.ShowFeedPanel();
                
            }
        }
        if (MainManager.singleton.OnTutorialStage == 3)
        {
            PointerPoo.SetActive(false);
            DialogManager.singleton.PlayMiniGameTutorialDialog();
            MainManager.singleton.OnTutorialStage = 4;
        }

    }
}
