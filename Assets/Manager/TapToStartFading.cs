using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TapToStartFading : MonoBehaviour
{
    public TextMeshProUGUI textMesh;
    public float blinkInterval = 0.5f;

    private float timer;
    private bool isTextVisible = true;
    public Coroutine FadeInCoroutine;
    public Coroutine FadeOutCoroutine;

    private void Start()
    {
        timer = blinkInterval;
    }

    private void Update()
    {
        timer -= Time.deltaTime;

        if (timer <= 0f)
        {
            ToggleTextVisibility();
            timer = blinkInterval;
        }
    }

    private void ToggleTextVisibility()
    {
        isTextVisible = !isTextVisible;
       // textMesh.gameObject.SetActive(isTextVisible);
       if (isTextVisible)
        {
           if(FadeInCoroutine != null) StopCoroutine(FadeInCoroutine);
            FadeOutCoroutine = StartCoroutine("FadeOut");
            
        }
       else
        {
            if (FadeOutCoroutine != null) StopCoroutine(FadeOutCoroutine);
            FadeInCoroutine = StartCoroutine("FadeIn");
        }
        
    }

    IEnumerator FadeIn ()
    {
        var t = 0.5f;
        var speed = 1 / t;
        var a = textMesh.color;
        while (a.a < 1f)
        {
            a.a += speed * Time.deltaTime;
            //CutsceneScreen.color += new Color(0, 0, 0, 0.01f);
            textMesh.color = a;
            yield return null;

        }
    }

    IEnumerator FadeOut()
    {
        var t = 0.5f;
        var speed = 1 / t;
        var a = textMesh.color;
        while (a.a > 0f)
        {
            a.a -= speed * Time.deltaTime;
            //CutsceneScreen.color += new Color(0, 0, 0, 0.01f);
            textMesh.color = a;
            yield return null;

        }
    }
}
