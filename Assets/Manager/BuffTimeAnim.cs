using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuffTimeAnim : MonoBehaviour
{
    public Image BuffTimeImage;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void OnEnable()
    {
        BuffTimeImage.fillAmount = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (BuffTimeImage.fillAmount > 0)BuffTimeImage.fillAmount -= 1/FoodRainingManager.singleton.ChangeTime * Time.deltaTime;
    }
}
