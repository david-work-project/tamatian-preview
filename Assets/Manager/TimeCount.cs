using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class TimeCount : MonoBehaviour
{
    public TMP_Text _playTimeText;
 
    void Update()
    {
        if(MainManager.singleton.PlayTimeLeft <=3.5f) _playTimeText.color = Color.red;
        _playTimeText.text = Math.Round(MainManager.singleton.PlayTimeLeft).ToString();
    }
}
