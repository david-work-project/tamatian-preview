using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingSceneManager : MonoBehaviour
{
    public static EndingSceneManager singleton;
    public GameObject EndPanel;
    public GameObject DeadPanel;
    // Start is called before the first frame update
    private void Awake()
    {
        singleton = this;
    }
    void Start()
    {
        if(MainManager.singleton.OnDead == true) ShowDeadPanel();
    }
    

    public void ShowEndPanel()
    {
        EndPanel.SetActive(true);
    }
    public void ShowDeadPanel()
    {
        DeadPanel.SetActive(true);
        MainManager.singleton.OnDead = false;
    }

    public void RestartGame()
    {
        MainManager.singleton.GoToGameplay();
    }


    public void ExitGame()
    {
        Application.Quit();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
