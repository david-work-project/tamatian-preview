using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RankBlog : MonoBehaviour
{
    public TextMeshProUGUI Rank;
    public TextMeshProUGUI Name;
    public TextMeshProUGUI Score;
    public GameObject Highlight;
    public GameObject CurrentSessionHighlight;
  
}
