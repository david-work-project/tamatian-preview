using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MeterGroup : MonoBehaviour
{
    public Image FeedMeter;
    public Image FeedMeterShadow;
    public GameObject FeedMeterEffect;
    public Image CleanMeter;
    public Image CleanMeterShadow;
    public GameObject CleanMeterEffect;
    public TextMeshProUGUI FeedTextUI;
    public TextMeshProUGUI DirtyTextUI;

    public Coroutine foodanimCoroutine;
    public Coroutine cleananimCoroutine;

    public float FillRate = 0.005f;
    public float FillInterval = 0.01f; 

    private void Start()
    {
        FeedTextUI.text = MainManager.singleton.Fullness.ToString()+"/100";
        DirtyTextUI.text = MainManager.singleton.Clean.ToString()+ "/100";
        FeedMeter.fillAmount = MainManager.singleton.Fullness / 100f;
        FeedMeterShadow.fillAmount = MainManager.singleton.Fullness / 100f;
        CleanMeter.fillAmount = MainManager.singleton.Clean / 100f;
        CleanMeterShadow.fillAmount = MainManager.singleton.Clean / 100f;
    }

    public void OnFeedMeterChange(float ChangeValue)
    {
        FeedMeterShadow.fillAmount = ChangeValue / 100f;
        FeedTextUI.text = ChangeValue.ToString() + "/100";
        if (foodanimCoroutine != null)
        {
            StopCoroutine(foodanimCoroutine);
        }
        foodanimCoroutine = StartCoroutine(FeedMeterAnimation());
    }

    IEnumerator FeedMeterAnimation()
    {
        FeedMeterEffect.SetActive(true);
        while (FeedMeter.fillAmount < FeedMeterShadow.fillAmount)
        {
            FeedMeter.fillAmount += 0.005f;
            yield return new WaitForSeconds(0.01f);
        }
        FeedMeterEffect.SetActive(false);
    }

    public void OnCleanMeterChange()
    {
        CleanMeterShadow.fillAmount = MainManager.singleton.Clean / 100f;
        DirtyTextUI.text = MainManager.singleton.Clean.ToString() + "/100";
        if (cleananimCoroutine != null)
        {
            StopCoroutine(cleananimCoroutine);
        }
        cleananimCoroutine = StartCoroutine(CleanMeterAnimation());
    }

    IEnumerator CleanMeterAnimation()
    {
        CleanMeterEffect.SetActive(true);
        while (CleanMeter.fillAmount < CleanMeterShadow.fillAmount)
        {
            CleanMeter.fillAmount += 0.005f;
            yield return new WaitForSeconds(0.01f);
        }
        CleanMeterEffect.SetActive(false);
    }

}
