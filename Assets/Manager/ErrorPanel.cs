using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ErrorPanel : MonoBehaviour
{
    public GameObject ReturnMenuButton;
    public GameObject QuitButton;
    private void OnEnable()
    {
        if (GameAPI.singleton.ErrorCount <2)
        {
            ReturnMenuButton.SetActive(true);
            
        }
        else
        {
            ReturnMenuButton.SetActive(false);
            QuitButton.SetActive(true);
        }
    }

    
    // Start is called before the first frame update
    void Start()
    {
        
    }
    

    // Update is called once per frame
    void Update()
    {
        
    }
}
