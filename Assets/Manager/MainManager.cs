using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TMPro;

using UnityEngine;
using UnityEngine.SceneManagement;

public class MainManager : MonoBehaviour
{
    public static MainManager singleton;
    [Header("Config FilePath")]
    public string UserfilePath = "Assets/taobingame_acc.yaml";
    public string configfilePath = "Assets/config_taobingame.yaml";
    public string ClientDeviceUserfilePath = "/mnt/sdcard/taobingame_acc.yaml";
    public string ClientDeviceConfigfilePath = "/mnt/sdcard/config_taobingame.yaml";

    [Header("Control State")]
    public bool gameEnded;
    public bool OnDead;
    public bool EnableTap = true;
    public bool FedThisLaunch = false;
    public bool OnPlayMiniGameThisLaunch = false;
    public bool OnPlayed = false;

    [Header("API Variable")]
    public int APIVersion;
    public string UserPhoneNumber;
    public int AccountLevel;
    public float AccountExp;
    public int[] AccountExpTable;
    public float TurtleExp;
    public int[] TurtleExpTable;
    public int OnTutorialStage;
    public int[] FedProgress;
    public int Fullness ;
    public int Clean = 100;
    public int PooCount;
    public bool _played_bonus;
    public bool PlayedToday;
    public int CurrentBackground;
    public int[] BackgroundCollection;
    public int TurtleType_ID;
    public string TurtleName;
    public int EvoStage;
    public int[] TurtleCollection;
    public int TaoCurrency;
    public string _box_id;
    public int RequestTimeOut;
    public int LastTurtleID;
    public List<RankData> OldrankData;
    public List<RankData> NewRankData;
    public List<RankData> ShowRankData;
    
    [Header("Global Variable")]
    public int ComboCount = 0;
    public int CutsceneToPlay;
    public float ChangedXp;
    public float MiniGameChangedXP;
    public float Score = 0;
    public int Level = 0;
    public float LevelProgress = 0;

    [Header("Info&Testing")]
    public bool TestMode;
    public GameObject InputNumber;
    public TMP_InputField InputField;
    public GameObject TestModeToggle;
    public GameObject ErrorShutdownPanel;
    public TextMeshProUGUI ErrorText;
    public TextMeshProUGUI VersionText;

    [Header("Config Variable Name")]
    public string EndPointConfigName = "API_ENDPOINT";
    public string PhoneNumberConfigName = "ACC";
    public string BoxIDConfigName = "BOX_ID";
    public string LanguageConfigName = "LANG";
    public string PlayTimeConfigName = "PLAY_TIME";
    public string RapidEvolveTimeConfigName = "RAPID_EVO_TIME";
    public string ResultTimeConfigName = "RESULT_TIME";
    public string MiniGameEndTimerConfigName = "ENDING_TIME";
    public string DeadTimerConfigName = "DEAD_TIME";
    public string EggHatchTimerConfigName = "EGG_HATCH_TIME";
    public string CloseIntroduceTimerConfigName = "CLOSE_INTRODUCE_TIME";
    public string BonusUncommonChanceConfigName = "UNCOMMON_CHANCE";
    public string BonusRareChanceConfigName = "RARE_CHANCE";
    public string BonusSpecialChanceConfigName = "SPECIAL_CHANCE";
    public string BonusRareCapConfigName = "RARE_CAP";
    public string BonusSpecialCapConfigName = "SPECIAL_CAP";
    public string BonusBadSpecialChanceConfigName = "BAD_SPECIAL_CHANCE";
    public string BonusBadSpecialCapConfigName = "BAD_SPECIAL_CAP";
    public string DefaultRequestTimeOutConfigName = "API_CONNECTION_TIMEOUT";
    public string CommonScoreMinConfigName = "COMMON_MIN";
    public string CommonScoreMaxConfigName = "COMMON_MAX";
    public string UncommonScoreMinConfigName = "UNCOMMON_MIN";
    public string UncommonScoreMaxConfigName = "UNCOMMON_MAX";
    public string RareScoreMinConfigName = "RARE_MIN";
    public string RareScoreMaxConfigName = "RARE_MAX";
    public string BadScoreConfigName = "BADSCORE";
    public string FirstStepComboConfigName = "FIRST_STEP_COMBO";
    public string SecondStepComboConfigName = "SECOND_STEP_COMBO";
    public string ThirdStepComboConfigName = "THIRD_STEP_COMBO";
    public string FourthStepComboConfigName = "FOURTH_STEP_COMBO";
    public string FifthStepComboConfigName = "FIFTH_STEP_COMBO";
    public string FirstStepComboFactorConfigName = "FIRST_STEP_COMBOBONUS_FACTOR";
    public string SecondStepComboFactorConfigName = "SECOND_STEP_COMBOBONUS_FACTOR";
    public string ThirdStepComboFactorConfigName = "THIRD_STEP_COMBOBONUS_FACTOR";
    public string FourthStepComboFactorConfigName = "FOURTH_STEP_COMBOBONUS_FACTOR";
    public string FifthStepComboFactorConfigName = "FIFTH_STEP_COMBOBONUS_FACTOR";
    public string FeedAmountConfigName = "FEED_AMOUNT";
    public string CleanAmountConfigName = "CLEAN_AMOUNT";
    public string FeedXpConfigName = "FEED_XP";
    public string SmallAmountOfPooMinConfigName = "SMALL_POO_MIN";
    public string MediumAmountOfPooMinConfigName = "MEDIUM_POO_MIN";
    public string LargeAmountOfPooConfigName = "LARGE_POO";

    [Header("Default Config Variable")]
    public string _language = "";

    public float PlayTimeLeft = 0f;
    public float ResultTime = 0f;
    public float RapidEvolveTime = 0f;
    public float MiniGameEndTimer = 0f;
    public float DeadTimer = 0f;
    public float EggHatchTimer = 0f;
    public float CloseIntroduceTimer = 0f;

    public float BonusUncommonChance = 0f;
    public float BonusRareChance = 0f;
    public float BonusSpecialChance = 0f;
    public float BonusRareCap = 0f;
    public float BonusRareCount  = 0f;
    public float BonusSpecialCap = 0f;
    public float BonusSpecialCount = 0f;
    public float BonusBadSpecialChance = 0f;
    public float BonusBadSpecialCap = 0f;
    public float BonusBadSpecialCount = 0f;
    
    public float CommonScoreMin = 0f;
    public float CommonScoreMax = 0f;
    public float UncommonScoreMin = 0f;
    public float UncommonScoreMax = 0f;
    public float RareScoreMin = 0f;
    public float RareScoreMax = 0f;
    public float BadScore = 0f;

    public float FirstStepCombo = 0f;
    public float SecondStepCombo = 0f;
    public float ThirdStepCombo = 0f;
    public float FourthStepCombo = 0f;
    public float FifthStepCombo = 0f;
    public float FirstStepComboFactor = 0f;
    public float SecondStepComboFactor = 0f;
    public float ThirdStepComboFactor = 0f;
    public float FourthStepComboFactor = 0f;
    public float FifthStepComboFactor = 0f;

    public float FeedAmount = 0f;
    public float CleanAmount = 0f;
    public float FeedXp = 0f;
    public int SmallAmountOfPooMin = 0;
    public int MediumAmountOfPooMin = 0;
    public int LargeAmountOfPoo = 0;
 
    public int DefaultBabyTurtleID = 3;

    public Coroutine TimeCoroutine;
    public GameObject TransitionScreen;
    public GameObject PanelNumber;

    
    void Awake()
    {
        singleton = this;
        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
    UserfilePath = ClientDeviceUserfilePath;
    configfilePath = ClientDeviceConfigfilePath;
    
#endif
        ReadDataFromConfig();
        VersionText.text = "ver. "+ Application.version;
       
    }

    public void ReceivedDataFromAPI()
    {
        var playerData = GameAPI.singleton.responsedplayerInfo.data;
        AccountLevel = playerData.account_level;
        AccountExp = playerData.account_exp;
        AccountExpTable = playerData.account_exp_table;
        TurtleExp = playerData.turtle_exp;
        TurtleExpTable = playerData.turtle_exp_table;
        OnTutorialStage = playerData.turtorial_stage;
        FedProgress = playerData.fed_progress;
        Fullness = playerData.health_point;
        Clean = playerData.clean_point;
        PooCount = playerData.poo_count;
        PlayedToday = playerData.played_today;
        CurrentBackground = playerData.current_background;
        BackgroundCollection = playerData.background_collection;
        TurtleType_ID = playerData.turtle_type_id;
        TurtleName = playerData.turtle_name;
        EvoStage = playerData.evo_stage;
        TurtleCollection = playerData.turtle_collection;
        TaoCurrency = playerData.tao_currency;
        LastTurtleID = playerData.last_turtle_type_id;
        OldrankData = playerData.ranker.ToList();
        ShowRankData = playerData.ranker.ToList();
        OnPlayed = true;
        if (OnTutorialStage == 0)
        {
            CutsceneToPlay = 0;
            CallCutscene();
        }
        else
        {
            GoToGameplay();
        }
    }

    private void ReadDataFromConfig()
    {
        GameAPI.singleton.apiurl = YAMLConfigReader.GetStringValueFromConfig(configfilePath, EndPointConfigName, DefaultVariable.singleton.DefaultEndPoint);
        UserPhoneNumber = YAMLConfigReader.GetStringValueFromConfig(UserfilePath, PhoneNumberConfigName, DefaultVariable.singleton.DefaultPhoneNumber);
        _box_id = YAMLConfigReader.GetStringValueFromConfig(configfilePath, BoxIDConfigName, DefaultVariable.singleton.DefaultBoxID);

        _language = YAMLConfigReader.GetStringValueFromConfig(UserfilePath, LanguageConfigName, DefaultVariable.singleton._language);
        ChangeObjectByLanguage[] langItem = FindObjectsOfType<ChangeObjectByLanguage>();
        foreach (ChangeObjectByLanguage obj in langItem)
        {
            Debug.Log(obj.gameObject.name);
            obj.Change();
        }

        PlayTimeLeft = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, PlayTimeConfigName, DefaultVariable.singleton.PlayTimeLeft);
        RapidEvolveTime = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, RapidEvolveTimeConfigName, DefaultVariable.singleton.RapidEvolveTime);
        ResultTime = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, ResultTimeConfigName, DefaultVariable.singleton.ResultTime);
        MiniGameEndTimer = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, MiniGameEndTimerConfigName, DefaultVariable.singleton.MiniGameEndTimer);
        DeadTimer = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, DeadTimerConfigName, DefaultVariable.singleton.DeadTimer);
        EggHatchTimer = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, EggHatchTimerConfigName, DefaultVariable.singleton.EggHatchTimer);
        CloseIntroduceTimer = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, CloseIntroduceTimerConfigName, DefaultVariable.singleton.CloseIntroduceTimer);
        BonusUncommonChance = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, BonusUncommonChanceConfigName, DefaultVariable.singleton.BonusUncommonChance);
        BonusRareChance = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, RapidEvolveTimeConfigName, DefaultVariable.singleton.BonusRareChance);
        BonusRareCap = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, BonusRareCapConfigName, DefaultVariable.singleton.BonusRareCap);
        BonusSpecialChance = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, BonusSpecialChanceConfigName, DefaultVariable.singleton.BonusSpecialChance);
        BonusSpecialCap = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, BonusSpecialCapConfigName, DefaultVariable.singleton.BonusSpecialCap);
        BonusBadSpecialChance = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, BonusBadSpecialChanceConfigName, DefaultVariable.singleton.BonusBadSpecialChance);
        BonusBadSpecialCap = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, BonusBadSpecialCapConfigName, DefaultVariable.singleton.BonusBadSpecialCap);
        RequestTimeOut = YAMLConfigReader.GetIntValueFromConfig(configfilePath, DefaultRequestTimeOutConfigName, DefaultVariable.singleton.DefaultRequestTimeOut);
       
        CommonScoreMin = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, CommonScoreMinConfigName, DefaultVariable.singleton.CommonScoreMin);
        CommonScoreMax = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, CommonScoreMaxConfigName, DefaultVariable.singleton.CommonScoreMax);
        UncommonScoreMin = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, UncommonScoreMinConfigName, DefaultVariable.singleton.UncommonScoreMin);
        UncommonScoreMax = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, UncommonScoreMaxConfigName, DefaultVariable.singleton.UncommonScoreMax);
        RareScoreMin = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, RareScoreMinConfigName, DefaultVariable.singleton.RareScoreMin);
        RareScoreMax = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, RareScoreMaxConfigName, DefaultVariable.singleton.RareScoreMax);
        BadScore = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, BadScoreConfigName, DefaultVariable.singleton.BadScore);
        
        FirstStepCombo = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, FirstStepComboConfigName, DefaultVariable.singleton.FirstStepCombo);
        SecondStepCombo = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, SecondStepComboConfigName, DefaultVariable.singleton.SecondStepCombo);
        ThirdStepCombo = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, ThirdStepComboConfigName, DefaultVariable.singleton.ThirdStepCombo);
        FourthStepCombo = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, FourthStepComboConfigName, DefaultVariable.singleton.FourthStepCombo);
        FifthStepCombo = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, FifthStepComboConfigName, DefaultVariable.singleton.FifthStepCombo);
        
        FirstStepComboFactor = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, FirstStepComboFactorConfigName, DefaultVariable.singleton.FirstStepComboFactor);
        SecondStepComboFactor = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, SecondStepComboFactorConfigName, DefaultVariable.singleton.SecondStepComboFactor);
        ThirdStepComboFactor = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, ThirdStepComboFactorConfigName, DefaultVariable.singleton.ThirdStepComboFactor);
        FourthStepComboFactor = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, FourthStepComboFactorConfigName, DefaultVariable.singleton.FourthStepComboFactor);
        FifthStepComboFactor = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, FifthStepComboFactorConfigName, DefaultVariable.singleton.FifthStepComboFactor);

        FeedAmount = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, FeedAmountConfigName, DefaultVariable.singleton.FeedAmount);
        CleanAmount = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, CleanAmountConfigName, DefaultVariable.singleton.CleanAmount);
        FeedXp = YAMLConfigReader.GetFloatValueFromConfig(configfilePath, FeedXpConfigName, DefaultVariable.singleton.FeedXp);
        SmallAmountOfPooMin = YAMLConfigReader.GetIntValueFromConfig(configfilePath, SmallAmountOfPooMinConfigName, DefaultVariable.singleton.SmallAmountOfPooMin);
        MediumAmountOfPooMin = YAMLConfigReader.GetIntValueFromConfig(configfilePath, MediumAmountOfPooMinConfigName, DefaultVariable.singleton.MediumAmountOfPooMin);
        LargeAmountOfPoo = YAMLConfigReader.GetIntValueFromConfig(configfilePath, LargeAmountOfPooConfigName, DefaultVariable.singleton.LargeAmountOfPoo);

        TimeCoroutine = StartCoroutine(Countdown());
        if (UserPhoneNumber == null || UserPhoneNumber == "")
        {
            EnableTap = false;
            PanelNumber.SetActive(true);
        }
        else
        {
            Invoke("LoadGameData", 3f);
        }
        LogManager.singleton.GenerateUserEnterLog(UserPhoneNumber);
    }

    public void ReceivedNumber()
    {
        EnableTap = true;
        Invoke("LoadGameData", 3f);
        LogManager.singleton.WriteUserLoginLog(UserPhoneNumber);
    }

    public void EnableTestMode(bool Testbool)
    {
        TestMode = Testbool;
        InputNumber.gameObject.SetActive(Testbool);
    }

    public void PlayGame()
    {
        if (EnableTap != true) return;
        if (UserPhoneNumber == "")
        {
            ShowErrorMessage("No Phone Number");
            return;
        }
        if (IsInvoking("LoadGameData"))
        {
            CancelInvoke("LoadGameData");
        }
        LoadGameData();
    }

    public void ShowErrorMessage(string errorMessage)
    {
      ErrorShutdownPanel.SetActive(true);
    }

    public void ErrorShutdown()
    {
        ErrorShutdownPanel.SetActive(true);
        Invoke("QuitApp", 5f);
    }

    public void GoToGameplay()
    {
       if(TestModeToggle != null) TestModeToggle.SetActive(false);
        if (IsInvoking("GoToGameplay"))
        {
            CancelInvoke("GoToGameplay");
        }
        SceneManager.LoadScene(2);
    }

    public void CallCutscene()
    {
        SceneManager.LoadScene(1);
    }

    IEnumerator Countdown()
    {
        while (PlayTimeLeft > 0)
        {
            yield return new WaitForSeconds(1f);
            PlayTimeLeft--;
        }
        ExitingGame();
    }

    public async void ExitingGame()
    {

        if (OnPlayed != true)
        {
            QuitApp();
            return;

        }
        else
        {
            await CalculateAndSetNewHighScore();
        }
        
        SaveGameData();
        StopCoroutine(TimeCoroutine);
        gameEnded = true;
        SceneManager.LoadScene(3);
        Invoke("QuitApp", 5f);
    }

    public void ShowDeadPanel()
    {
        SceneManager.LoadScene(3);
        Invoke("GoToGameplay", 5f);
    }

    public void QuitApp()
    {
        Application.Quit();
    }
    void LoadGameData()
    {
        if (OnTutorialStage == 0)
        {
            CutsceneToPlay = 0;
            CallCutscene();
        }
        else
        {
            GoToGameplay();
        }
        // GameAPI.singleton.RequestPlayerData();
    }

     void SaveGameData()
    {   
        GameAPI.singleton.SavePlayerData();
    }

    public async Task CalculateAndSetNewHighScore()
    {

        NewRankData = OldrankData;
        RankData newRankData = new RankData();
        newRankData.score = Mathf.FloorToInt(Score);
        newRankData.mobile_number = UserPhoneNumber;
        for (int i = 0; i < OldrankData.Count; i++)
        {
            if (Score > OldrankData[i].score)
            {
                newRankData.rank = i + 1;
                NewRankData.Insert(i, newRankData);
                NewRankData.RemoveAt(5);
                for (int j = i + 1; j < OldrankData.Count; j++)
                {
                    NewRankData[j].rank += 1;
                }
                SetShowRankData();
                return;
            }
        }
    }

        public void SetShowRankData()
    {
        ShowRankData = NewRankData;
        GameAPI.singleton.SaveRankData(); 
    }
     
}
