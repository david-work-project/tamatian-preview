using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameplayManager : MonoBehaviour
{
    public static GameplayManager singleton;

    [Header("Vairable")]
    public float CurrentLevelMAX;
    public int nextSpawn;
    
    [Header("State Condition")]
    public bool PooPickupPhase = true;
    public bool isDead = false;
    public bool isEvolve = false;
    public bool isOnCutscene = false;
    public bool onDialog = false;
    public bool OnQuitConfirmPanel = false;

    [Header("Egg")]
    public GameObject egg;
    public GameObject IntroduceDialog;
    public GameObject TapToHatch;
    public GameObject EggSpawned;
    public ParticleSystem EggHatchingParticle;

    [Header("UI Management")]
    public MeterGroup AllMeterGroup;
    public GameObject Pointer;
    public GameObject PointerEgg;
    public GameObject FeedPanel;
    public ScoreIndicatorControl ScoreIndicator;
    public LevelIndicator LevelIndicator;
    public HighscoreIndicator HighScoreIndicator;
    public Image EvolFilter;
    SpriteRenderer currentBGSprite;
    public SpriteRenderer[] BGS;
    public GameObject ExitPanel;
    public MiniGameXPIndicator XPIndicator;



    [Header("Food Management")]
    public GameObject[] Food;
    
    public Coroutine evolvingCoroutine;
    public Coroutine evolvingFilterCoroutine;
    public Coroutine changeSpriteCoroutine;
    public Coroutine foodanimCoroutine;
    public Coroutine cleananimCoroutine;
    public Coroutine levelProgressCoroutine;

    public Material CharacterMaterial;
    public GameplaySFX SFX;

   
    
    void Awake()
    {
        singleton = this;
    }
    void Start()
    {
        CharacterMaterial.color = new Color();
        
            if (MainManager.singleton.OnTutorialStage <= 3)
            {
                switch (MainManager.singleton.OnTutorialStage)
                {
                    case 0:
                    case 1:
                    TutorialFirstStage();
                        break;
                    case 2:
                    case 3:
                        OnGoingTutorialStage();
                        break;
                }
            }
            else
            {
                if (MainManager.singleton.TurtleType_ID >= 2)
                {
                    InitGamePlay();
                }
                else
                {
                    InitEggWithoutTutorial();

                }
            }
    }

    public void CheckLevelProgress()
    {
        MainManager.singleton.Level = Pet.instance.EvoStage;
        if (Pet.instance.EvoStage >= 1)
        {
            var levelIndex = Pet.instance.EvoStage - 1;
            currentBGSprite = Instantiate(BGS[levelIndex], transform);
            LevelIndicator.LevelEmblemImage.sprite = LevelIndicator.LevelEmblemSprites[levelIndex];
            LevelIndicator.LevelTextImage.sprite = LevelIndicator.LevelTextSprite[levelIndex];
            CurrentLevelMAX = MainManager.singleton.TurtleExpTable[levelIndex];
            LevelIndicator.LevelProgressTextUI.text = MainManager.singleton.TurtleExp.ToString("F1") + "/" + CurrentLevelMAX.ToString();
            LevelIndicator.LevelProgressUIImage.fillAmount = MainManager.singleton.TurtleExp / CurrentLevelMAX;
            LevelIndicator.LevelProgressUIAnim.fillAmount = MainManager.singleton.TurtleExp / CurrentLevelMAX;

        }
        else if (Pet.instance.EvoStage == 0)
        {
            currentBGSprite = Instantiate(BGS[0], transform);
            LevelIndicator.LevelEmblemImage.sprite = LevelIndicator.LevelEmblemSprites[0];
            LevelIndicator.LevelTextImage.sprite = LevelIndicator.LevelTextSprite[0];
            CurrentLevelMAX = 1;
            LevelIndicator.LevelProgressTextUI.text = "1/1";
            LevelIndicator.LevelProgressUIImage.fillAmount = 1f;
            LevelIndicator.LevelProgressUIAnim.fillAmount = 1f;
        }
        LevelIndicator.gameObject.SetActive(true);
    }

    private void InitEggWithoutTutorial()
    {
        SpawnEgg();
        AllMeterGroup.gameObject.SetActive(false); 
        MainManager.singleton.Fullness = 100;
        PointerEgg.SetActive(true);
        Invoke("EggHatching", MainManager.singleton.EggHatchTimer);
    }
    public void OnPetLoaded()
    {
        CheckLevelProgress();
        if (MainManager.singleton.OnTutorialStage <= 3)
        {
            if (MainManager.singleton.OnTutorialStage == 2)
            {
                DialogManager.singleton.FeedTutorialDialog();
            }
            else if (MainManager.singleton.OnTutorialStage == 3)
            {
                FeedPanel.SetActive(true);
            }
        }
        else
        {
            if (MainManager.singleton.Fullness <= 0)
            {
                DeadDialog();
                isDead = true;
                Pet.instance.DialogBalloon.SkipButton.SetActive(false);
                return;
            }
            if (MainManager.singleton.Clean == 0)
            {
                DialogManager.singleton.LargeAmountOfPooDialog();
                EvolveToPooForm(); 
                return;
            }
            if (MainManager.singleton.FedThisLaunch == false && isDead == false) { 
                if(MainManager.singleton.PooCount > 0)
                {
                    ShowDialogByPooCount();
                }
                else
                {
                    ShowFeedPanel();
                }
            }
            
        }
        if (MainManager.singleton.FedThisLaunch)
        {
            PooPickupPhase = false;
            if (MainManager.singleton.OnPlayMiniGameThisLaunch == true)
            {
                CallRainDown();
                ScoreIndicator.gameObject.SetActive(true);
                ScoreIndicator.ReceivedScore(0);
            }
            else
            {
                DialogManager.singleton.PlayMiniGameDialog();
            }
        }
    }

    void ShowDialogByPooCount()
    {
        if(Pet.instance.EvoStage == 0)
        {
            DialogManager.singleton.PoolFormDialog();
        }
        if(MainManager.singleton.PooCount >= MainManager.singleton.LargeAmountOfPoo){
            DialogManager.singleton.LargeAmountOfPooDialog();
            return;
        }
        else if (MainManager.singleton.PooCount >= MainManager.singleton.MediumAmountOfPooMin)
        {
            DialogManager.singleton.MediumAmountOfPooDialog();
            return;
        }
        else if (MainManager.singleton.PooCount >= MainManager.singleton.SmallAmountOfPooMin)
        {
            DialogManager.singleton.SmallAmountOfPooDialog();
            return;
        }
    }

    private void TutorialFirstStage()
    {
        SpawnEgg();
        onDialog = true;
        MainManager.singleton.OnTutorialStage = 1;
        IntroduceDialog.SetActive(true);
        AllMeterGroup.gameObject.SetActive(false);
        MainManager.singleton.Fullness = 0;
        if (MainManager.singleton.TurtleCollection[0] == 0) MainManager.singleton.TurtleCollection[0] = 1;
        if (MainManager.singleton.BackgroundCollection[0] == 0) MainManager.singleton.BackgroundCollection[0] = 1;
        Invoke("CloseIntroduceDialog", MainManager.singleton.CloseIntroduceTimer);
    }
    private void OnGoingTutorialStage()
    {
        PetManager.singleton.LoadPet(3);
        AllMeterGroup.gameObject.SetActive(true);
        LevelIndicator.gameObject.SetActive(true);
    }

    void DeadDialog()
    {
        PooPickupPhase = false;
        DialogManager.singleton.OnDeadDialog();
        Invoke("DeadCutScene", MainManager.singleton.DeadTimer);
    }
    public void DeadCutScene()
    {
        isDead = false;
        GameAPI.singleton.ResetGame();
        MainManager.singleton.OnDead = true;
        MainManager.singleton.TurtleExp = 0;
        MainManager.singleton.FedProgress = new int[5] { 0, 0, 0, 0, 0 };
        MainManager.singleton.PooCount = 0;
        MainManager.singleton.FedThisLaunch = false;
        MainManager.singleton.Clean = 100;
        MainManager.singleton.TurtleType_ID = 1;
        MainManager.singleton.CutsceneToPlay = 3;
        MainManager.singleton.CallCutscene();
    }

    public void ShowFeedPanel()
    {
        if (MainManager.singleton.FedThisLaunch) return;
        FeedPanel.SetActive(true);
        Pet.instance.DialogBalloon.gameObject.SetActive(false);
    }

    private void InitGamePlay()
    {
        PetManager.singleton.LoadPet(MainManager.singleton.TurtleType_ID);
        PooManager.singleton.SpawnPoo(MainManager.singleton.PooCount);
        AllMeterGroup.gameObject.SetActive(true);
    }

    private void SpawnEgg()
    {
        currentBGSprite = Instantiate(BGS[0], transform);
        if (MainManager.singleton.TurtleCollection[0] == 0) MainManager.singleton.TurtleCollection[0] = 1;
        EggSpawned = Instantiate(egg, transform);
        TapToHatch.SetActive(true);
    }

    public void CloseIntroduceDialog()
    {
        if(IsInvoking("CloseIntroduceDialog")) CancelInvoke("CloseIntroduceDialog");
        onDialog = false;
        IntroduceDialog.SetActive(false);
        PointerEgg.SetActive(true);
        Invoke("EggHatching", MainManager.singleton.EggHatchTimer);
    }

    public void EggHatching()
    {
        if (IsInvoking("EggHatching")) CancelInvoke("EggHatching");
        if (TapToHatch) TapToHatch.SetActive(false);
        if(PointerEgg)PointerEgg.SetActive(false);

        Destroy(EggSpawned.gameObject);
        EggHatchingParticle.Play();
        MainManager.singleton.TurtleType_ID = MainManager.singleton.DefaultBabyTurtleID;
        if (MainManager.singleton.OnTutorialStage <= 3)
        {
            if (MainManager.singleton.OnTutorialStage == 1)
            {
                MainManager.singleton.OnTutorialStage = 2;
                OnGoingTutorialStage();
            }
        }
        else
        {
            InitGamePlay();
        }
    }
    public void CallRainDown()
    {
        PooPickupPhase = false;
        FoodRainingManager.singleton.StartRainDown();
        HighScoreIndicator.CurrentHighScore.text = MainManager.singleton.OldrankData[4].score.ToString();
        MainManager.singleton._played_bonus = true;
        MainManager.singleton.OnPlayMiniGameThisLaunch = true;
        HighScoreIndicator.gameObject.SetActive(true);
        XPIndicator.gameObject.SetActive(true);
    }

    public void CompleteFeeding()
    {
        SFX.CompleteFeedingSound.Play();
        if ( MainManager.singleton.OnTutorialStage <= 3)
        {
            MainManager.singleton.Fullness = 100;
            MainManager.singleton.OnTutorialStage = 3;
            PooManager.singleton.SpawnPooForTutorial();
            DialogManager.singleton.CleanTutorialDialog();
        }
        else
        {
            MainManager.singleton.Fullness += 20;
            if (MainManager.singleton.Fullness > 100) MainManager.singleton.Fullness = 100;
            DialogManager.singleton.PlayMiniGameDialog();
        }
        if (MainManager.singleton.OnPlayMiniGameThisLaunch != true)
        {
            if (Pet.instance.EvoStage > 0)
            {
                if(MainManager.singleton.TurtleExp < CurrentLevelMAX)
                {
                    var SumFeedXP = MainManager.singleton.TurtleExp + MainManager.singleton.FeedXp ;
                    if(SumFeedXP < CurrentLevelMAX)
                    {
                        PopFeedXpEffect(MainManager.singleton.FeedXp);
                    }
                    else
                    {
                        var _excess = SumFeedXP - CurrentLevelMAX;
                        PopFeedXpEffect(MainManager.singleton.FeedXp - _excess);
                    }
                    
                }
                else
                {
                    ChangeLevelProgress(0);
                    PopFeedXpEffect(0);
                }
                
            }
            else
            {
                
            }
        }
        AllMeterGroup.OnFeedMeterChange(MainManager.singleton.Fullness);

        if (MainManager.singleton.Clean > 50 && Pet.instance.EvoStage == 0)
        {
            ReturnToOldForm();
        }
        ChangeLevelProgress(MainManager.singleton.FeedXp);
       
        
    }
    public void SpawnFood(int _foodIndex)
    {
        MainManager.singleton.FedThisLaunch = true;
        if (FeedPanel) FeedPanel.SetActive(false);
        MainManager.singleton.FedProgress[_foodIndex] += 1;
        SFX.DrinkSound.Play();
        Instantiate(Food[_foodIndex], transform);
    }

    public IEnumerator Evolving()
    {
        while (currentBGSprite.color.r > 0)
        {
            CharacterMaterial.color += new Color(0.0025f, 0.0025f, 0.0025f, 0);
            currentBGSprite.color -= new Color(0.0025f, 0.0025f, 0.0025f, 0);
            yield return new WaitForSeconds(0.01f);
        }
    }
    public void EvolCutscene()
    {
        EggHatchingParticle.Play();
        if (Pet.instance.EvoStage ==1 || nextSpawn == 2 )
        {
            MainManager.singleton.CutsceneToPlay = 1;
            MainManager.singleton.CallCutscene();
        }
        else if (Pet.instance.EvoStage == 2)
        {
            MainManager.singleton.CutsceneToPlay = 2;
            MainManager.singleton.CallCutscene();
        }

    }
    public IEnumerator FadeInFilter()
    {
        var t = 2f;
        var speed = 1 / t;
        var a = EvolFilter.color;
        while (a.a < 1f)
        {
            a.a += speed * Time.deltaTime;

            EvolFilter.color = a;
            yield return null;
        }
    }

    public int FindFedProgressHighestValue(int[] fedprogress)
    {
        int highestIndex = -1;
        int highestValue = int.MinValue;
        List<int> highestIndices = new List<int>();
        int feedsum = 0;
        for (int i = 0; i < fedprogress.Length; i++)
        {
            feedsum += fedprogress[i];
            if (fedprogress[i] > highestValue)
            {
                highestValue = fedprogress[i];
                highestIndex = i;
                highestIndices.Clear();
                highestIndices.Add(i);
            }
            else if (fedprogress[i] == highestValue)
            {
                highestIndices.Add(i);
            }
        }
        if (feedsum < 5) return -1;
        if(highestIndices.Count > 0)
        {
            int[] priority = { 4, 3, 2, 1, 0 };
            highestIndices.Sort((x, y) => Array.IndexOf(priority, x) - Array.IndexOf(priority, y));
            return highestIndices[0];
        }
        int randomIndex = highestIndices.Count>0 ? highestIndices[Random.Range(0, highestIndices.Count)]: highestValue;
        return randomIndex;
    }
    public void EvolveCheck()
    {
      
        int MostDrinkType = FindFedProgressHighestValue(MainManager.singleton.FedProgress);
        if (MostDrinkType < 0)
        {
           if(MainManager.singleton.OnPlayMiniGameThisLaunch != true) DialogManager.singleton.NeedMoreDrinkDialog();
            return;
        }
        switch (Pet.instance.EvoStage)
        {
            case 1:
                nextSpawn = PetManager.singleton.SearchEvolving(2, MostDrinkType);
                
                if (nextSpawn < 0) return;
                MainManager.singleton.TurtleType_ID = nextSpawn;
                MainManager.singleton.TurtleExp %= CurrentLevelMAX;
                GoToEvolve();
                break;
            case 2:         
                nextSpawn = PetManager.singleton.SearchEvolving(3, MostDrinkType);
                if (nextSpawn < 0) return;
                MainManager.singleton.TurtleType_ID = nextSpawn;
                MainManager.singleton.TurtleExp %= CurrentLevelMAX;
                GoToEvolve();
                break;
            case 3:
                return;

        }
        MainManager.singleton.FedProgress = new int[5] { 0, 0, 0, 0, 0 };

    }

    public void GoToEvolve()
    {
        if (MainManager.singleton.PlayTimeLeft >= MainManager.singleton.RapidEvolveTime)
        {
                isOnCutscene = true;
                FeedPanel.SetActive(false);
            //    Pet.instance.DialogBalloon.SkipButton.SetActive(false);
            //    Pet.instance.DialogBalloon.PlayButton.SetActive(false);
                Pet.instance.DialogBalloon.gameObject.SetActive(false);
                evolvingCoroutine = StartCoroutine(Evolving());
                Invoke("StartFilter", 0.5f);
                Invoke("EvolCutscene", 3f);
        }
        else
        {
            isOnCutscene = true;
            FeedPanel.SetActive(false);
          //  Pet.instance.DialogBalloon.SkipButton.SetActive(false);
          //  Pet.instance.DialogBalloon.PlayButton.SetActive(false);
            Pet.instance.DialogBalloon.gameObject.SetActive(false);
            evolvingFilterCoroutine = StartCoroutine(FadeInFilter());
            Invoke("QuickEvo", 3f);
        }
    }

    public void EvolveToPooForm()
    {
        if (Pet.instance.EvoStage <= 0) return;
        nextSpawn = PetManager.singleton.PooFormId;
        MainManager.singleton.TurtleType_ID = nextSpawn;
        GoToEvolve();
    }

    public void ReturnToOldForm()
    {
        nextSpawn = MainManager.singleton.LastTurtleID;
        if (MainManager.singleton.LastTurtleID <= 0) nextSpawn = MainManager.singleton.DefaultBabyTurtleID;
        MainManager.singleton.TurtleType_ID = nextSpawn;
        isOnCutscene = true;
        evolvingFilterCoroutine = StartCoroutine(FadeInFilter());
        Invoke("QuickEvo", 3f);
    }

    public void QuickEvo()
    {
        isOnCutscene = false;
        if (currentBGSprite) currentBGSprite.gameObject.SetActive(false);
        PetManager.singleton.LoadPet(MainManager.singleton.TurtleType_ID);
        EggHatchingParticle.Play();
        StopCoroutine(evolvingFilterCoroutine);
        EvolFilter.color = new Color();
    }

    public void ChangeLevelProgress(float _IncreseLevelProgressPoint)
    {
        if (levelProgressCoroutine != null) StopCoroutine(levelProgressCoroutine);
        if (MainManager.singleton.TurtleExp <= 0) MainManager.singleton.TurtleExp = 0;
        if (Pet.instance.EvoStage > 0)
        {
            if (MainManager.singleton.TurtleExp < CurrentLevelMAX)
            {
                MainManager.singleton.TurtleExp += _IncreseLevelProgressPoint;
              
                if (MainManager.singleton.TurtleExp >= CurrentLevelMAX)
                {
                    var _excess = MainManager.singleton.TurtleExp - CurrentLevelMAX;
                    MainManager.singleton.ChangedXp += _IncreseLevelProgressPoint-_excess;
                    MainManager.singleton.TurtleExp = CurrentLevelMAX;
                    EvolveCheck();
                }
                else
                {
                    MainManager.singleton.ChangedXp += _IncreseLevelProgressPoint;
                }
            }
            else
            {
                EvolveCheck();
            }
            
            LevelIndicator.OnXpChange();
        }
        else
        {
                EvolveCheck();
        }
    }

    void PopFeedXpEffect(float RecievedValue)
    {
        ScoreIndicator.gameObject.SetActive(true);
        ScoreIndicator.RecievedFeedXP(RecievedValue);
        ScoreIndicator.ScoreAnim.Play("FadeOut");
    }

    public void UpdateMiniGameXPChange(float RecievedValue)
    {
       
        if (Pet.instance.EvoStage > 0)
        {
            if (MainManager.singleton.TurtleExp < CurrentLevelMAX)
            {
                var XpCombineWithReceive = MainManager.singleton.MiniGameChangedXP + RecievedValue;
                if (XpCombineWithReceive > CurrentLevelMAX)
                {
                    MainManager.singleton.MiniGameChangedXP += XpCombineWithReceive - CurrentLevelMAX;
                }
                else
                {
                    MainManager.singleton.MiniGameChangedXP += RecievedValue;
                }

            }
            XPIndicator.XPText.text = MainManager.singleton.MiniGameChangedXP.ToString("F1") + " XP";
        }
        else
        {
            XPIndicator.XPText.text = "0 XP";
        }
    }

}
