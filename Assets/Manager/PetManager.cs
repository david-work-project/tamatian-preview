using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class PetManager : MonoBehaviour
{
    public static PetManager singleton;
    
    [System.Serializable]
    public struct PetData
    {
        public int PetId;
        public string PrefabName;
        public string PetNameEN;
        public string PetNameTH;
        public string Detail;
        public int EvoStage;
        public PetType Type;
        public PetType SubType1;
        public PetType SubType2;
        public PetType SubType3;
    }
    public PetData[] PetList;

    public int PooFormId;

    private void Awake()
    {
        singleton = this;
    }
    public void LoadPet(int PetId)
    {
        PetData petData = Array.Find(PetList, pet => pet.PetId == PetId);
        if (petData.PetId == 0)
        {
            Debug.LogError("Pet not found");
            return;
        }
        if (Pet.instance) Destroy(Pet.instance.gameObject);
        Addressables.LoadAssetAsync<GameObject>(petData.PrefabName).Completed += OnLoadPetComplete;
        if (PetId - 1 >= MainManager.singleton.TurtleCollection.Length)
        {
            // do nothing
        }
        else
        {
            if (MainManager.singleton.TurtleCollection[PetId - 1] == 0) MainManager.singleton.TurtleCollection[PetId - 1] = 1;
        }
    }

    void OnLoadPetComplete(AsyncOperationHandle<GameObject> obj)
    {
        if (obj.Status == AsyncOperationStatus.Succeeded)
        {
           
            GameObject TurtleSpawned = Instantiate(obj.Result, transform);
            GameplayManager.singleton.OnPetLoaded();
        }
        else
        {
            Debug.LogError("Failed to load pet");
        }
    }

    public int SearchEvolving(int stage,int fedType)
    { 
        Debug.Log("Searching for stage " + stage + " fed with " + fedType);
        var fedTypeIndex = fedType + 1;
        int IndexToSpawn = -1;
        foreach (PetData petdata in PetList) 
        {
            if (petdata.EvoStage == stage)
            {
                if (stage - 1 >= MainManager.singleton.BackgroundCollection.Length)
                {
                    // do nothing
                }
                else
                {
                    if (MainManager.singleton.BackgroundCollection[stage - 1] == 0) MainManager.singleton.BackgroundCollection[stage - 1] = 1;
                }
                switch (stage)
                {
                    case 2:
                        if (petdata.Type == (PetType)fedTypeIndex)
                        {
                            IndexToSpawn = petdata.PetId;
                            Debug.Log("Evolving to " + petdata.PetId);
                            return IndexToSpawn;
                        }
                        break;
                    case 3:
                        if (petdata.EvoStage == 3)
                        {
                            if (petdata.Type == Pet.instance.Type)
                            {
                                if (petdata.SubType1 == (PetType)fedTypeIndex || petdata.SubType2 == (PetType)fedTypeIndex || petdata.SubType3 == (PetType)fedTypeIndex)
                                {
                                    IndexToSpawn = petdata.PetId;
                                    Debug.Log("Evolving to " + petdata.PetId);
                                    return IndexToSpawn;
                                }
                            }
                        }
                        break;
                }
            }
        }
        return IndexToSpawn;
    }
}
