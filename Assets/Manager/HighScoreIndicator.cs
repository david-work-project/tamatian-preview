using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HighscoreIndicator : MonoBehaviour
{
    public TextMeshProUGUI Label;
    public TextMeshProUGUI Rank;
    public TextMeshProUGUI CurrentHighScore;

    public string[] RankText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
