using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UpdateXPTextPosition : UIBehaviour
{
    // Start is called before the first frame update
    public RectTransform ScoreTextRect;
    public RectTransform XPGainTextRect;
    public float Margin;
    void Start()
    {
        
    }
    protected override void OnRectTransformDimensionsChange()
    {
        base.OnRectTransformDimensionsChange();
        // Call your function here
        UpdatetPosition();
    }
    public void UpdatetPosition()
    {
       
        XPGainTextRect.anchoredPosition = new Vector3(ScoreTextRect.anchoredPosition.x + ScoreTextRect.rect.width / 2 + Margin, XPGainTextRect.anchoredPosition.y, 0);
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
