using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultVariable : MonoBehaviour
{
    public static DefaultVariable singleton;

    [Header("Default API Config Variable")]
    public string DefaultEndPoint = "";
    public string DefaultPhoneNumber = "";
    public string DefaultBoxID = "000000";
    public string _language = "TH";
    public int DefaultRequestTimeOut = 8;

    [Header("Default Timer Variable")]
    public float PlayTimeLeft = 60f;
    public float ResultTime = 5f;
    public float RapidEvolveTime = 14f;
    public float MiniGameEndTimer = 5f;
    public float DeadTimer = 4f;
    public float EggHatchTimer = 14f;
    public float CloseIntroduceTimer = 6f;

    [Header("Default Bonus Chance&Cap Variable")]
    public float BonusUncommonChance = 20f;
    public float BonusRareChance = 4f;
    public float BonusSpecialChance = 3f;
    public float BonusRareCap = 8f;
    public float BonusSpecialCap = 2f;
    public float BonusBadSpecialChance = 7f;
    public float BonusBadSpecialCap = 3f;

    [Header("Default Score Variable")]
    public float CommonScoreMin = 100f;
    public float CommonScoreMax = 110f;
    public float UncommonScoreMin = 500f;
    public float UncommonScoreMax = 525f;
    public float RareScoreMin = 800f;
    public float RareScoreMax = 850f;
    public float BadScore = -50f;

    [Header("Default Combo Variable")]
    public float FirstStepCombo = 10f;
    public float SecondStepCombo = 20f;
    public float ThirdStepCombo = 30f;
    public float FourthStepCombo = 40f;
    public float FifthStepCombo = 50f;
    public float FirstStepComboFactor = 1.2f;
    public float SecondStepComboFactor = 1.4f;
    public float ThirdStepComboFactor = 1.6f;
    public float FourthStepComboFactor = 1.8f;
    public float FifthStepComboFactor = 2f;

    [Header("Default Gameplay Variable")]
    public float FeedAmount = 20f;
    public float CleanAmount = 35f;
    public float FeedXp = 5f;
    public int SmallAmountOfPooMin = 1;
    public int MediumAmountOfPooMin = 3;
    public int LargeAmountOfPoo = 5;

    public int DefaultBabyTurtleID = 3;

    private void Awake()
    {
        singleton = this;
    }
}
