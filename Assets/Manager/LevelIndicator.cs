using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelIndicator : MonoBehaviour
{
    public Sprite[] LevelEmblemSprites;
    public Image LevelEmblemImage;
    public Sprite[] LevelTextSprite;
    public Image LevelTextImage;
    public GameObject LevelProgressEffect;
    public TextMeshProUGUI LevelProgressTextUI;
    public Image LevelProgressUIImage;
    public Image LevelProgressUIAnim;
    public Coroutine LevelProgressChangeCoroutine;
    public float XPChangeAnimInterval = 0.01f;
    public float XPChangeAnimSpeed = 0.005f;
    // Start is called before the first frame update
    public void OnXpChange()
    {
        
        if(LevelProgressChangeCoroutine != null)
        {
            StopCoroutine(LevelProgressChangeCoroutine);
        }
        LevelProgressTextUI.text = MainManager.singleton.TurtleExp.ToString("F1")+"/" + GameplayManager.singleton.CurrentLevelMAX.ToString();
        LevelProgressUIAnim.fillAmount = MainManager.singleton.TurtleExp / GameplayManager.singleton.CurrentLevelMAX;
        LevelProgressChangeCoroutine = StartCoroutine(FillProgressAnimation());
    }

    public IEnumerator FillProgressAnimation()
    {
        
        LevelProgressEffect.SetActive(true);
        while (LevelProgressUIImage.fillAmount < LevelProgressUIAnim.fillAmount)
        {

            LevelProgressUIImage.fillAmount += XPChangeAnimSpeed;
            if (LevelProgressUIImage.fillAmount > LevelProgressUIAnim.fillAmount)
            {
                LevelProgressUIImage.fillAmount = LevelProgressUIAnim.fillAmount;
            }
            yield return new WaitForSeconds(XPChangeAnimInterval);

        }

        while (LevelProgressUIImage.fillAmount > LevelProgressUIAnim.fillAmount)
        {
            LevelProgressUIImage.fillAmount -= XPChangeAnimSpeed;
            if (LevelProgressUIImage.fillAmount < LevelProgressUIAnim.fillAmount)
            {
                LevelProgressUIImage.fillAmount = LevelProgressUIAnim.fillAmount;
            }

            yield return new WaitForSeconds(XPChangeAnimInterval);

        }
        LevelProgressEffect.SetActive(false);
    }

    
}
