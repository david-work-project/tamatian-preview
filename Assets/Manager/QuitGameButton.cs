using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGameButton : MonoBehaviour
{
   public void ShowQuitPanel()
    {
        GameplayManager.singleton.ExitPanel.SetActive(true);
        GameplayManager.singleton.OnQuitConfirmPanel = true;
    }
}
