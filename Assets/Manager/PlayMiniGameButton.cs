using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayMiniGameButton : MonoBehaviour
{
    public void GoToMiniGameButton()
    {
        GameplayManager.singleton.CallRainDown();
        gameObject.SetActive(false);
        gameObject.transform.parent.gameObject.SetActive(false);
    }
}
