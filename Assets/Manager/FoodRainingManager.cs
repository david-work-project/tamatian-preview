using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public class FoodRainingManager : MonoBehaviour
{
    public static FoodRainingManager singleton;
    public float FoodSpawnInterval = 0.15f;
    public float _bonusScore = 0;

    private Coroutine foodSpawningCoroutine;
    private Coroutine badSpawningCoroutine;

    public float RemainingTime ;

    public GameObject RemainingTimeUI;
    public TextMeshProUGUI RemainingTimeText;

    public GameObject[] NormalFoods;
    public GameObject[] UncommonFoods;
    public GameObject[] RareFoods;
    public GameObject[] BadFoods;
    public GameObject[] Specials;
    public GameObject GoodParticle;
    public GameObject BadParticle;
    public GameObject Score;
    public float ScoreMultiplier = 1f;

    public Color PositiveScoreColor;
    public Color ComboColorStep1;
    public Color ComboColorStep2;
    public Color ComboColorStep3;
    public Color ComboColorStep4;
    public Color ComboColorStep5;
    public Color NegativeScoreColor;
    public Color CurrentColor;

    public GameObject FoodSpawnArea;
    public float UncommonChance = 20;
    public float RareChance = 10;
    public float SpecialChance = 3;
    public float BadSpecialChance = 7;
    public float FirstSpecialThreshold = 3;
    public bool SpawnSpecial = false;
    bool IsChangeToGood = false;
    bool IsChangeToBad = false;
    public float ChangeTime = 5f;
    public float ItemCooldown = 6f;
    public float RareCap = 8;
    public float RareSpawnCount = 0;
    public float SpecialCap = 1;
    public float SpecialSpawnCount = 0;
    public int ComboCount= 0;
    
    float itemCooldown;
    Vector2 FoodMovement;
    public GameObject Buff;
    public GameObject Debuff;

    public int ParticlePoolSize = 10;
    public int SpecialPoolSize = 2;
    private Queue<GameObject> GoodParticlePool;
    private Queue<GameObject> BadParticlePool;
    private Queue<GameObject> ScorePool; 
    private Queue<GameObject> FoodPool;
    private Queue<GameObject> UncommonFoodPool;
    private Queue<GameObject> RareFoodPool;
    private Queue<GameObject> BadFoodPool;
    private Queue<GameObject> SpecialPool;
    private Queue<GameObject> BadSpecialPool;

    void Awake()
    {
        singleton = this;
        
    }
    void Start()
    {
        InitPools();
    }

    public void InitPools()
    {
        GoodParticlePool = new Queue<GameObject>();
        BadParticlePool = new Queue<GameObject>();
        ScorePool = new Queue<GameObject>();
        FoodPool = new Queue<GameObject>();
        UncommonFoodPool = new Queue<GameObject>();
        RareFoodPool = new Queue<GameObject>();
        BadFoodPool = new Queue<GameObject>();
        SpecialPool = new Queue<GameObject>();
        BadSpecialPool = new Queue<GameObject>();

        for (int i = 0; i < ParticlePoolSize; i++)
        {
            GameObject goodParticle = Instantiate(GoodParticle);
            GameObject badParticle = Instantiate(BadParticle);
            GameObject score = Instantiate(Score);
            goodParticle.gameObject.SetActive(false);
            badParticle.gameObject.SetActive(false);
            score.gameObject.SetActive(false);
            GoodParticlePool.Enqueue(goodParticle);
            BadParticlePool.Enqueue(badParticle);
            ScorePool.Enqueue(score);

        }
        var foodPoolSize = NormalFoods.Length;
        for (int i = 0; i < foodPoolSize; i++)
        {
            GameObject food = Instantiate(NormalFoods[i]);
            food.SetActive(false);
            FoodPool.Enqueue(food);
        }
        var uncommonFoodPoolSize = UncommonFoods.Length;
        for (int i = 0; i < uncommonFoodPoolSize; i++)
        {
            GameObject food = Instantiate(UncommonFoods[i]);
            food.SetActive(false);
            UncommonFoodPool.Enqueue(food);
        }
        var rareFoodPoolSize = RareFoods.Length;
        for (int i = 0; i < rareFoodPoolSize; i++)
        {
            GameObject food = Instantiate(RareFoods[i]);
            food.SetActive(false);
            RareFoodPool.Enqueue(food);
        }
        var badFoodPoolSize = BadFoods.Length;
        for (int i = 0; i < badFoodPoolSize; i++)
        {
            GameObject food = Instantiate(BadFoods[i]);
            food.SetActive(false);
            BadFoodPool.Enqueue(food);
        }
        
        for (int i = 0; i < SpecialPoolSize; i++)
        {
            GameObject special = Instantiate(Specials[0]);
            special.SetActive(false);
            SpecialPool.Enqueue(special);

            GameObject Badspecial = Instantiate(Specials[1]);
            Badspecial.SetActive(false);
            BadSpecialPool.Enqueue(Badspecial);
        }
    }

    public void StartRainDown()
    {
        SpawnSpecial = false;
        RemainingTime = MainManager.singleton.PlayTimeLeft;
        Pet.instance.DialogBalloon.gameObject.SetActive(false);
        if (RemainingTime <= MainManager.singleton.MiniGameEndTimer * 2)
        {
            Invoke("StopRainDown", RemainingTime -= RemainingTime / 2f);
        }
        else
        {
            Invoke("StopRainDown", RemainingTime -= MainManager.singleton.MiniGameEndTimer);
        }


        Invoke("StartSpawnSpecial", FirstSpecialThreshold);
        foodSpawningCoroutine = StartCoroutine(FoodSpawning());
        badSpawningCoroutine = StartCoroutine(BadSpawning());
    }

    public void StartSpawnSpecial()
    {
        SpawnSpecial = true;
    }
    public void Freeze(float FreezeTimer)
    {

        BonusItems[] referencingObjects = FindObjectsOfType<BonusItems>();

        foreach (BonusItems referencingObject in referencingObjects)
        {
            referencingObject.Freeze();
            Debug.Log("Object found: " + referencingObject.gameObject.name);
        }

        RemainingTime += FreezeTimer;
        StopCoroutine(foodSpawningCoroutine);
        StopCoroutine(badSpawningCoroutine);
        Invoke("UnFreeze", FreezeTimer);
    }

    public void ChangeToGood()
    {
        Buff.SetActive(true);
        Freeze(0.25f);
        IsChangeToGood = true; 
        BonusItems[] referencingObjects = FindObjectsOfType<BonusItems>();

        foreach (BonusItems referencingObject in referencingObjects)
        {
            if(referencingObject._myFullness < 0 && !referencingObject.SpecialGood)
            {
                referencingObject.TurnToGood();
            }
            Debug.Log("Object found: " + referencingObject.gameObject.name);
        }
        Invoke("StopChangeToGood", ChangeTime);

    }

    public void ChangeToBad()
    {
        Debuff.SetActive(true);
        Freeze(0.25f);
        IsChangeToBad = true;
        BonusItems[] referencingObjects = FindObjectsOfType<BonusItems>();

        foreach (BonusItems referencingObject in referencingObjects)
        {
            if (referencingObject._myFullness > 0 && !referencingObject.SpecialBad)
            {
                referencingObject.TurnToBad();
            }
           
        }
        Invoke("StopChangeToBad", ChangeTime);

    }

    public void StopChangeToGood()
    {
        Buff.SetActive(false);
        IsChangeToGood = false;
    }

    public void StopChangeToBad()
    {
        Debuff.SetActive(false);
        IsChangeToBad = false;
    }

    public void UnFreeze()
    {
        RemainingTime = MainManager.singleton.PlayTimeLeft;

        BonusItems[] referencingObjects = FindObjectsOfType<BonusItems>();

        foreach (BonusItems referencingObject in referencingObjects)
        {
            referencingObject.Unfreeze();
          
        }
        if (RemainingTime > 10)
        {
            foodSpawningCoroutine = StartCoroutine(FoodSpawning());
            badSpawningCoroutine = StartCoroutine(BadSpawning());
        }
    }

    public void GetParticleFromPool(string poolname, Vector3 SpawnPosition)
    {
        switch (poolname)
        {
            case  "GoodParticle":
                if (GoodParticlePool.Count > 0)
                {
                    GameObject goodParticle = GoodParticlePool.Dequeue();
                    goodParticle.transform.position = SpawnPosition;
                    goodParticle.SetActive(true);
                    goodParticle.GetComponent<ParticleSystem>().Play();

                }
                else
                {
                    GameObject goodParticle = Instantiate(GoodParticle);
                    goodParticle.transform.position = SpawnPosition;
                    goodParticle.SetActive(true);
                    goodParticle.GetComponent<ParticleSystem>().Play();
                }
                break;
            case "BadParticle":
                if (BadParticlePool.Count > 0)
                {
                    GameObject badParticle = BadParticlePool.Dequeue();
                    badParticle.transform.position = SpawnPosition;
                    badParticle.SetActive(true);
                    badParticle.GetComponent<ParticleSystem>().Play();
                }
                else
                {
                    GameObject badParticle = Instantiate(BadParticle);
                    badParticle.transform.position = SpawnPosition;
                    badParticle.SetActive(true);
                    badParticle.GetComponent<ParticleSystem>().Play();
                }
                break;
        }
    }

    public void ReturnParticleToPool(string poolname, GameObject particle)
    {
        switch (poolname)
        {
            case "GoodParticle":
                particle.gameObject.SetActive(false);
                GoodParticlePool.Enqueue(particle);
                break;
            case "BadParticle":
                particle.gameObject.SetActive(false);
                BadParticlePool.Enqueue(particle);
                break;
        }
    }

    public void GetSpecialFromPool(string poolname, Vector3 SpawnPosition)
    {
        switch (poolname)
        {
            case "Special":
                if (SpecialPool.Count > 0)
                {
                    GameObject special = SpecialPool.Dequeue();
                    special.transform.position = SpawnPosition;
                    special.SetActive(true);
                }
                else
                {
                    GameObject special = Instantiate(Specials[0]);
                    special.transform.position = SpawnPosition;
                    special.SetActive(true);
                }
                break;
            case "SpecialBad":
                if (BadSpecialPool.Count > 0)
                {
                    GameObject specialBad = BadSpecialPool.Dequeue();
                    specialBad.transform.position = SpawnPosition;
                    specialBad.SetActive(true);
                }
                else
                {
                    GameObject specialBad = Instantiate(Specials[1]);
                    specialBad.transform.position = SpawnPosition;
                    specialBad.SetActive(true);
                }
                break;
        }
    }
    public void GetFoodsFromPool(string poolname, Vector3 SpawnPosition)
    {
        switch (poolname)
        {
            case "NormalFood":
                if (FoodPool.Count > 0)
                {
                    GameObject food = FoodPool.Dequeue();
                    food.transform.position = SpawnPosition;
                    food.SetActive(true);
                }
                else
                {
                    var i = Random.Range(0, NormalFoods.Length);
                    GameObject food = Instantiate(NormalFoods[i]);
                    food.transform.position = SpawnPosition;
                    food.SetActive(true);
                }
                break;
            case "UncommonFood":
                if (UncommonFoodPool.Count > 0)
                {
                    GameObject uncommon = UncommonFoodPool.Dequeue();
                    uncommon.transform.position = SpawnPosition;
                    uncommon.SetActive(true);
                }
                else
                {
                    var i = Random.Range(0, UncommonFoods.Length);
                    GameObject uncommon = Instantiate(UncommonFoods[i]);
                    uncommon.transform.position = SpawnPosition;
                    uncommon.SetActive(true);
                }
                break;
            case "RareFood":

                if (RareFoodPool.Count > 0)
                {
                    GameObject rare = RareFoodPool.Dequeue();
                    rare.transform.position = SpawnPosition;
                    rare.SetActive(true);

                }
                else
                {
                    var i = Random.Range(0, RareFoods.Length);
                    GameObject rare = Instantiate(RareFoods[i]);
                    rare.transform.position = SpawnPosition;
                    rare.SetActive(true);
                }
                break;

            case "BadFood":

                if (BadFoodPool.Count > 0)
                {
                    GameObject bad = BadFoodPool.Dequeue();
                    bad.transform.position = SpawnPosition;
                    bad.SetActive(true);

                }
                else
                {
                    var i = Random.Range(0, BadFoods.Length);
                    GameObject bad = Instantiate(BadFoods[i]);
                    bad.transform.position = SpawnPosition;
                    bad.SetActive(true);
                }
                break;
        }
    }

    public void ReturnFoodsToPool(string poolname, GameObject food)
    {
        switch (poolname)
        {
            case "NormalFood":
                food.gameObject.SetActive(false);
                FoodPool.Enqueue(food);
                break;
            case "UncommonFood":
                food.gameObject.SetActive(false);
                UncommonFoodPool.Enqueue(food);
                break;
            case "RareFood":
                food.gameObject.SetActive(false);
                RareFoodPool.Enqueue(food);
                break;
            case "BadFood":
                food.gameObject.SetActive(false);
                BadFoodPool.Enqueue(food);
                break;
        }
    }

    public void ReturnSpecialsToPool(string poolname, GameObject special)
    {
        switch (poolname)
        {
            case "Special":
                special.gameObject.SetActive(false);
                SpecialPool.Enqueue(special);
                break;
            case "BadSpecial":
                special.gameObject.SetActive(false);
                BadSpecialPool.Enqueue(special);
                break;
        }
    }

    public void GetScoreParticleFromPool(float scoreNumber, Vector3 SpawnPosition)
    {
        if (ScorePool.Count > 0)
        {
            GameObject scoreP = ScorePool.Dequeue();
            
            scoreP.transform.position = SpawnPosition;
            scoreP.GetComponent<ScoreParticle>().Text.text = scoreNumber.ToString();
            if (scoreNumber > 0)
            {
                scoreP.GetComponent<ScoreParticle>().Text.color = CurrentColor;
            }
            else
            {
                scoreP.GetComponent<ScoreParticle>().Text.color = NegativeScoreColor;
            }
            scoreP.SetActive(true);

        }
        else
        {
            GameObject scoreP = Instantiate(Score);
            scoreP.GetComponent<ScoreParticle>().Text.text = scoreNumber.ToString();
            if (scoreNumber > 0)
            {
                scoreP.GetComponent<ScoreParticle>().Text.color = CurrentColor;
            }
            else
            {
                scoreP.GetComponent<ScoreParticle>().Text.color = NegativeScoreColor;
            }
            scoreP.transform.position = SpawnPosition;
           
            
        }
    }

    public void ReturnScoreParticleToPool(GameObject score)
    {
        score.SetActive(false);
        ScorePool.Enqueue(score);
    }

    public void StopRainDown()
    {
        StopCoroutine(foodSpawningCoroutine);
        StopCoroutine(badSpawningCoroutine);
    }

    public void BonusUpdate(float IncomingBonusPoint)
    {
        _bonusScore += IncomingBonusPoint;
        if (_bonusScore <= 0) _bonusScore = 0;
    }

    public IEnumerator FoodSpawning()
    {
        while (true)
        {
            if (IsChangeToBad == false)
            {
                SpawnFood();
            }
          
            if(IsChangeToGood)
            {
                yield return new WaitForSeconds(FoodSpawnInterval*0.75f);
            }
            else
            {
                yield return new WaitForSeconds(FoodSpawnInterval);
            }
        }
    }

    public IEnumerator BadSpawning()
    {
        while (true)
        {
            if(IsChangeToGood == false)
            {
                spawnBadFood();
            }

            if (IsChangeToBad)
            {
                yield return new WaitForSeconds(FoodSpawnInterval *0.75f);
            }
            else
            {
                yield return new WaitForSeconds(FoodSpawnInterval * 1.75f);
            }
        }
    }

    public void SpawnFood()
    {
        Vector3 randomPosition = new Vector3(
            Random.Range(FoodSpawnArea.transform.position.x - FoodSpawnArea.transform.localScale.x / 2f, FoodSpawnArea.transform.position.x + FoodSpawnArea.transform.localScale.x / 2f),
            Random.Range(FoodSpawnArea.transform.position.y - FoodSpawnArea.transform.localScale.y / 2f, FoodSpawnArea.transform.position.y + FoodSpawnArea.transform.localScale.y / 2f),
            0
            );

        float Proc = Random.Range(0, 100);
        if ( Proc < MainManager.singleton.BonusSpecialChance )
        {     
            if (SpawnSpecial && IsChangeToGood == false && IsChangeToBad == false && Time.time > itemCooldown && MainManager.singleton.BonusSpecialCount < MainManager.singleton.BonusSpecialCap)
            {
                GetSpecialFromPool("Special", randomPosition);
                MainManager.singleton.BonusSpecialCount += 1;
                itemCooldown = Time.time + ItemCooldown;
            }
            else
            {
                GetFoodsFromPool("UncommonFood", randomPosition);
            }
        }
        
        else if (Proc < MainManager.singleton.BonusRareChance && Proc > MainManager.singleton.BonusSpecialChance)
        {
            if (MainManager.singleton.BonusRareCount < MainManager.singleton.BonusRareCap)
            {
                GetFoodsFromPool("RareFood", randomPosition);
               MainManager.singleton.BonusRareCount += 1;
            }
            else{
                GetFoodsFromPool("UncommonFood", randomPosition);
            }
        }
        else if (Proc > MainManager.singleton.BonusRareChance && Proc < MainManager.singleton.BonusUncommonChance)
        {
            GetFoodsFromPool("UncommonFood", randomPosition);
        }
        else if (Proc > MainManager.singleton.BonusUncommonChance )
        {
            GetFoodsFromPool("NormalFood", randomPosition);
        }
    }

    public void spawnBadFood()
    {
        Vector3 randomPosition = new Vector3(
            Random.Range(FoodSpawnArea.transform.position.x - FoodSpawnArea.transform.localScale.x / 2f, FoodSpawnArea.transform.position.x + FoodSpawnArea.transform.localScale.x / 2f),
            Random.Range(FoodSpawnArea.transform.position.y - FoodSpawnArea.transform.localScale.y / 2f, FoodSpawnArea.transform.position.y + FoodSpawnArea.transform.localScale.y / 2f),
            0
            );
        float Proc = Random.Range(0, 100);
        if (Proc < MainManager.singleton.BonusBadSpecialChance)
        {
            if (SpawnSpecial && IsChangeToGood == false && IsChangeToBad == false && Time.time > itemCooldown && MainManager.singleton.BonusBadSpecialCount < MainManager.singleton.BonusBadSpecialCap)
            {
                GetSpecialFromPool("BadSpecial", randomPosition);
                MainManager.singleton.BonusBadSpecialCount += 1;
                itemCooldown = Time.time + ItemCooldown;
            }
            else
            {
                int BadFood = Random.Range(0, BadFoods.Length);
                GetFoodsFromPool("BadFood", randomPosition);
            }
        }
        else
        {
            int BadFood = Random.Range(0, BadFoods.Length);
            GetFoodsFromPool("BadFood", randomPosition);
        }
    }

    public void UpdateScore(float score)
    {
        if (ComboCount < MainManager.singleton.FirstStepCombo)
        {
            ScoreMultiplier = 1f;
            CurrentColor = PositiveScoreColor;
        }
        if (ComboCount >= MainManager.singleton.FirstStepCombo)
        {
            ScoreMultiplier = MainManager.singleton.FirstStepComboFactor;
            CurrentColor = ComboColorStep1;
        }
        if (ComboCount >= MainManager.singleton.SecondStepCombo)
        {
            ScoreMultiplier = MainManager.singleton.SecondStepComboFactor;
            CurrentColor = ComboColorStep2;
        }
        if (ComboCount >= MainManager.singleton.ThirdStepCombo)
        {
            ScoreMultiplier = MainManager.singleton.ThirdStepComboFactor;
            CurrentColor = ComboColorStep3;
        }
        if (ComboCount >= MainManager.singleton.FourthStepCombo)
        {
            ScoreMultiplier = MainManager.singleton.FourthStepComboFactor;
            CurrentColor = ComboColorStep4;
        }
        if (ComboCount >= MainManager.singleton.FifthStepCombo)
        {
            ScoreMultiplier = MainManager.singleton.FifthStepComboFactor;
            CurrentColor = ComboColorStep5;
        }

        MainManager.singleton.Score += score * ScoreMultiplier;
        GameplayManager.singleton.ScoreIndicator.gameObject.SetActive(true);
        GameplayManager.singleton.ScoreIndicator.XPText.SetActive(false);
        GameplayManager.singleton.ScoreIndicator.GainText.SetActive(false);
        GameplayManager.singleton.ScoreIndicator.MultiplyText.gameObject.SetActive(true);
        GameplayManager.singleton.ScoreIndicator.MultiplyText.text = "x" + ScoreMultiplier.ToString();
        GameplayManager.singleton.ScoreIndicator.MultiplyText.color = CurrentColor;
        GameplayManager.singleton.ScoreIndicator.MultiplyLabel.gameObject.SetActive(true);
        GameplayManager.singleton.ScoreIndicator.ComboLabel.gameObject.SetActive(true);
        GameplayManager.singleton.ScoreIndicator.ComboText.gameObject.SetActive(true);
        GameplayManager.singleton.ScoreIndicator.ReceivedScore(score);

        if (MainManager.singleton.Score > MainManager.singleton.OldrankData[4].score)
        {
            GameplayManager.singleton.HighScoreIndicator.CurrentHighScore.text = MainManager.singleton.OldrankData[3].score.ToString();
            GameplayManager.singleton.HighScoreIndicator.Rank.gameObject.SetActive(true);
            GameplayManager.singleton.HighScoreIndicator.Rank.text = "Now You're Rank 5";
        }
        if (MainManager.singleton.Score > MainManager.singleton.OldrankData[3].score)
        {
            GameplayManager.singleton.HighScoreIndicator.CurrentHighScore.text = MainManager.singleton.OldrankData[2].score.ToString();
            GameplayManager.singleton.HighScoreIndicator.Rank.text = "Now You're Rank 4";
        }
        if (MainManager.singleton.Score > MainManager.singleton.OldrankData[2].score)
        {
            GameplayManager.singleton.HighScoreIndicator.CurrentHighScore.text = MainManager.singleton.OldrankData[1].score.ToString();
            GameplayManager.singleton.HighScoreIndicator.Rank.text = "Now You're Rank 3";
        }
        if (MainManager.singleton.Score > MainManager.singleton.OldrankData[1].score)
        {
            GameplayManager.singleton.HighScoreIndicator.CurrentHighScore.text = MainManager.singleton.OldrankData[0].score.ToString();
            GameplayManager.singleton.HighScoreIndicator.Rank.text = "Now You're Rank 2";
        }
        if (MainManager.singleton.Score > MainManager.singleton.OldrankData[0].score)
        {
            GameplayManager.singleton.HighScoreIndicator.CurrentHighScore.text = Mathf.Floor(MainManager.singleton.Score).ToString();
            GameplayManager.singleton.HighScoreIndicator.Rank.text = "Now You're Rank 1";
        }
    }

}
