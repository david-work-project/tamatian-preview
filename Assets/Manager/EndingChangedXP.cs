using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EndingChangedXP : MonoBehaviour
{
    public TextMeshProUGUI ScoreText;
    public TextMeshProUGUI ProgressText;
    public Color PositiveColor;
    public Color NegativeColor;
    public string XpGainWording = "And you gain";

    private void OnEnable()
    {
       
        var finalXPChange = MainManager.singleton.ChangedXp ;
        if (finalXPChange >= 0)
        {
            ProgressText.text = XpGainWording +" <color=#" + ColorUtility.ToHtmlStringRGB(PositiveColor) + "><size=150%>"+ finalXPChange.ToString("F1")+ "</size ></color>  XP";
          
        }
        else
        {
            ProgressText.text = XpGainWording + " <color=#" + ColorUtility.ToHtmlStringRGB(NegativeColor) + "><size=150%>" + finalXPChange.ToString("F1") + "</size></color>  XP";
      
        }
       
        ScoreText.text = MainManager.singleton.Score.ToString();

    }

}
