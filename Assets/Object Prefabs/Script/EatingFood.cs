using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EatingFood : MonoBehaviour
{
    public float _myFullness = 0;
    public float EatTime = 0;
    public bool ate = false;
    void Start()
    {
        
    }

    void OnFeedComplete()
    {
        GameplayManager.singleton.CompleteFeeding();
        
        Destroy(this.gameObject);
    }

    void Update()
    {
        if (!ate)
        {
            EatTime += Time.deltaTime;
            if (EatTime >= 2)
            {
                OnFeedComplete();
                ate = true;
            }
        }
    }
}
