using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Unity.VisualScripting;
using UnityEngine;

public class FeedButton : MonoBehaviour
{

    public void FeedButtonClick()
    {
        GameplayManager.singleton.ShowFeedPanel();
        GameplayManager.singleton.Pointer.gameObject.SetActive(false);
        gameObject.SetActive(false);
        gameObject.transform.parent.gameObject.SetActive(false);

    }
}
