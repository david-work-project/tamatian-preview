using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickableObject : MonoBehaviour
{
    // Implement Clickbehavior here
    public virtual void Oncollect()
    {
        
        Destroy(this.gameObject);

    }

    public virtual void OnMouseDown()
    {
      
        Oncollect();
    }


}
