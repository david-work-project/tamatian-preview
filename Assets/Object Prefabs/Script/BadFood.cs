using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadFood : ClickableObject
{
    
    public override void Oncollect()
    {
    //    GameplayManager.singleton.BonusUpdate(-10);

        base.Oncollect();


    }

    private void Update()
    {
        this.transform.Rotate(0, 0, 45 * Time.deltaTime);
        if (this.transform.position.y <= -6)
        {
            Destroy(this.gameObject);
        }
    }
    public override void OnMouseDown()
    {
        if (MainManager.singleton.gameEnded) return;
        Oncollect();
    }

}
