using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public class Pet : MonoBehaviour
{
    public static Pet instance;
    public DialogBalloon DialogBalloon;
    public TextMeshProUGUI dialogText;

    public int PetId;
    public PetType Type;
    public PetType SubType;
    public int EvoStage;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
    }
}
