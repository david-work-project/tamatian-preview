using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Egg : MonoBehaviour
{
    public int TapToHatch = 3;
    bool EnableTap = true;
    public Animator EggAnim;
    public bool OnHatch = false;
    public ParticleSystem ClickParticle;
   
    // Start is called before the first frame update
    void Start()
    {
       
    }
    void OnMouseDown()
    {
        if (GameplayManager.singleton.onDialog) return;
        if (EnableTap)
        {
            ClickParticle.Play();
            if (TapToHatch > 0)
            {
                CrackingAnim();
            }
            else
            {   
                HatchingAnim();
            }
        }

    }

    void CrackingAnim()
    {
        EnableTap = false;
       
            EggAnim.SetTrigger("Hatching");
     //  if(GameplayManager.singleton.PointerEgg) GameplayManager.singleton.PointerEgg.SetActive(false);
        TapToHatch -= 1;
            Invoke("ReEnableTap", 1.25f);
        
       

        
      

    }


    void ReEnableTap()
    {
        EnableTap = true;
    }

    void HatchingAnim()
    {
        OnHatch = true;
        EnableTap = false;
        EggAnim.SetTrigger("Hatching");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
