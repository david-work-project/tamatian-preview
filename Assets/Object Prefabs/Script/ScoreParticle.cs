using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreParticle : MonoBehaviour
{
    // Start is called before the first frame update
    public float TimeToReturn = 0.3f;
    public TextMeshPro Text;

    void OnEnable()
    {
        Invoke("ReturnToPool", TimeToReturn);
    }

    void ReturnToPool()
    {
        FoodRainingManager.singleton.ReturnScoreParticleToPool(gameObject);
    }

}
