using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class BonusItems : MonoBehaviour
{
    
    public float _myFullness = 0;
    public GameObject ScoreParticle;
    public GameObject ParticleSpawned;
    public Rigidbody2D Rigidbody;
    public float OldGravityScale;
    public float OldMass;
    public bool SpecialGood = false;
    public bool SpecialBad = false;
    public ScoreParticle ScoreEffect;
    public float maxVelocityY;
    float rotate;
    public float Rotate = 25f; 
    bool freeze = false;
    public float myScore;
    public enum FoodType
    {
        Normal,
        Uncommon,
        Rare,
        Bad,
        Special,
        SpecialBad
    }
    public FoodType MyType;

    public void Start()
    {
        rotate = Random.Range(-Rotate, Rotate);
        OldGravityScale = Rigidbody.gravityScale;
        OldMass = Rigidbody.mass;       
        
    }
    public void Oncollect()
    {
        if (GameplayManager.singleton.isOnCutscene) return;
        if (SpecialGood)
        {
            FoodRainingManager.singleton.ChangeToGood();
        }
        if (SpecialBad)
        {
            FoodRainingManager.singleton.ChangeToBad();
        }
    
        GameplayManager.singleton.UpdateMiniGameXPChange(_myFullness);
        
        switch (MyType)
        {
            case FoodType.Normal:
                GameplayManager.singleton.ChangeLevelProgress(_myFullness);
                FoodRainingManager.singleton.GetParticleFromPool("GoodParticle", transform.position);
                FoodRainingManager.singleton.ComboCount += 1;
                myScore =Mathf.Floor(Random.Range(MainManager.singleton.CommonScoreMin, MainManager.singleton.CommonScoreMax));
                FoodRainingManager.singleton.UpdateScore(myScore);
                break;
            case FoodType.Uncommon:
                GameplayManager.singleton.ChangeLevelProgress(_myFullness);
                FoodRainingManager.singleton.GetParticleFromPool("GoodParticle", transform.position);
                FoodRainingManager.singleton.ComboCount+=1;
                myScore = Mathf.Floor(Random.Range(MainManager.singleton.UncommonScoreMin, MainManager.singleton.UncommonScoreMax));
                FoodRainingManager.singleton.UpdateScore(myScore);
                break;
            case FoodType.Rare:
                GameplayManager.singleton.ChangeLevelProgress(_myFullness);
                FoodRainingManager.singleton.GetParticleFromPool("GoodParticle", transform.position);
                FoodRainingManager.singleton.ComboCount+=1;
                myScore = Mathf.Floor(Random.Range(MainManager.singleton.RareScoreMin, MainManager.singleton.RareScoreMax));
                FoodRainingManager.singleton.UpdateScore(myScore);
                break;
            case FoodType.Bad:
                GameplayManager.singleton.ChangeLevelProgress(_myFullness);
                FoodRainingManager.singleton.GetParticleFromPool("BadParticle", transform.position);
                FoodRainingManager.singleton.ComboCount = 0;
                myScore = MainManager.singleton.BadScore;
                FoodRainingManager.singleton.UpdateScore(myScore);
                break;
            case FoodType.Special:
                GameplayManager.singleton.ChangeLevelProgress(_myFullness);
                FoodRainingManager.singleton.GetParticleFromPool("GoodParticle", transform.position);
                break;
            case FoodType.SpecialBad:
                GameplayManager.singleton.ChangeLevelProgress(_myFullness);
                FoodRainingManager.singleton.GetParticleFromPool("BadParticle", transform.position);
                FoodRainingManager.singleton.ComboCount = 0;
                break;
        }
        ReturnToTheirPoolByCollect();

    }

    public void ReturnToTheirPoolByCollect()
    {
        var ShowingScore = Mathf.Floor(myScore * FoodRainingManager.singleton.ScoreMultiplier);
        switch (MyType)
        {
            case FoodType.Normal:
                FoodRainingManager.singleton.GetScoreParticleFromPool(ShowingScore, transform.position);
                FoodRainingManager.singleton.ReturnFoodsToPool("NormalFood", gameObject);
              
                break;
            case FoodType.Uncommon:
                FoodRainingManager.singleton.GetScoreParticleFromPool(ShowingScore, transform.position);
                FoodRainingManager.singleton.ReturnFoodsToPool("UncommonFood", gameObject);
               
                break;
            case FoodType.Rare:
                FoodRainingManager.singleton.GetScoreParticleFromPool(ShowingScore, transform.position);
                FoodRainingManager.singleton.ReturnFoodsToPool("RareFood", gameObject);
                
                break;
            case FoodType.Bad:
                FoodRainingManager.singleton.GetScoreParticleFromPool(myScore, transform.position);
                FoodRainingManager.singleton.ReturnFoodsToPool("BadFood", gameObject);
               
                break;
            case FoodType.Special:
                FoodRainingManager.singleton.ReturnSpecialsToPool("Special", gameObject);
                break;
            case FoodType.SpecialBad:
                FoodRainingManager.singleton.ReturnSpecialsToPool("SpecialBad", gameObject);
                break;
        }
    }

     public void ReturnToTheirPool()
    { 
        switch (MyType)
        {
            case FoodType.Normal:         
                FoodRainingManager.singleton.ReturnFoodsToPool("NormalFood", gameObject);        
                break;
            case FoodType.Uncommon:         
                FoodRainingManager.singleton.ReturnFoodsToPool("UncommonFood", gameObject);
                break;
            case FoodType.Rare:
                FoodRainingManager.singleton.ReturnFoodsToPool("RareFood", gameObject);
                break;
            case FoodType.Bad:
                FoodRainingManager.singleton.ReturnFoodsToPool("BadFood", gameObject);
                break;
            case FoodType.Special:
                FoodRainingManager.singleton.ReturnSpecialsToPool("Special", gameObject);
                break;
            case FoodType.SpecialBad:
                FoodRainingManager.singleton.ReturnSpecialsToPool("SpecialBad", gameObject);
                break;
        }
    }
    public void Freeze()
    {
        freeze = true;
        Rigidbody.gravityScale = 0;
        Rigidbody.velocity = new Vector2(0, 0);
        Rigidbody.mass = 0;
        Rigidbody.isKinematic = true;

    }

    public void Unfreeze()
    {
        freeze = false;
        Rigidbody.gravityScale = OldGravityScale;
        Rigidbody.mass = OldMass;
        Rigidbody.isKinematic = false;

    }

    public void TurnToGood()
    {
        Vector2 Currentposition = this.transform.position;
        int Goodfood = UnityEngine.Random.Range(0, FoodRainingManager.singleton.NormalFoods.Length);
        Instantiate(FoodRainingManager.singleton.NormalFoods[Goodfood], Currentposition, Quaternion.identity);
        ReturnToTheirPool();

    }

    public void TurnToBad()
    {
        Vector2 Currentposition = this.transform.position;
        int BadFood = UnityEngine.Random.Range(0, FoodRainingManager.singleton.BadFoods.Length);
        Instantiate(FoodRainingManager.singleton.BadFoods[BadFood], Currentposition, Quaternion.identity);
       ReturnToTheirPool();
    }


    private void FixedUpdate()
    {
        if (!freeze) this.transform.Rotate(0, 0, rotate * Time.deltaTime);
        if (this.transform.position.y <= -6)
        {
            ReturnToTheirPool();
        }
        if(Mathf.Abs(Rigidbody.velocity.y) > maxVelocityY)
        {
            Vector2 newVelocity = Rigidbody.velocity;
            newVelocity.y = Mathf.Sign(Rigidbody.velocity.y) * maxVelocityY;
            Rigidbody.velocity = newVelocity;
        }
    }

    public void OnMouseDown()
    {
        if (MainManager.singleton.gameEnded || GameplayManager.singleton.OnQuitConfirmPanel) return;
        Oncollect();
    }
}
