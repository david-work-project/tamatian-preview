using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneTranstion : MonoBehaviour
{
   
    // Start is called before the first frame update
    
    void Start()
    {
        
    }

    private void OnEnable()
    {
        this.gameObject.transform.position = new Vector3(0, 0,-8);
        Invoke("DisableTransition", 0.5f);
    }

    void DisableTransition()
    {
        this.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        this.gameObject.transform.position += new Vector3(-25, 0, 0) * Time.deltaTime;
    }
}
