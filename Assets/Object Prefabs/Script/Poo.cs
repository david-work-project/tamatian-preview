using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Poo : ClickableObject
{
    public GameObject PooParticle;
    public GameObject ParticleSpawned;

    public override void Oncollect()
    {
        if (MainManager.singleton.OnTutorialStage >= 4 && GameplayManager.singleton.PooPickupPhase != true) return;
        if (GameplayManager.singleton.isOnCutscene) return;
        PooManager.singleton.PickUpPoo();

        GameObject newObject = Instantiate(PooParticle); 
        newObject.transform.position = transform.position;
        newObject.transform.rotation = Quaternion.identity;
        base.Oncollect();

    }

}
